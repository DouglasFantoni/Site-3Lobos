import {Box} from "@mui/system";
import NovelCardHorizontal from "../../../ui/components/NovelCard/NovelCardHorizontal";
import {ChapterFull} from "../../../models/chapter";
import {Grid} from "@mui/material";

interface Props {
    chapters: string,
}

export default function LastChapters({chapters}: Props) {
    const newChapters = JSON.parse(chapters) as Array<ChapterFull>;

    return (
        <Box sx={{m:-2, }}>
            <Grid container spacing={2} >
                {newChapters.map((el, index) => (
                    <Grid key={index} item  sm={4} xs={12}>
                        <NovelCardHorizontal chapter={el}/>
                    </Grid>
                ))}
            </Grid>
        </Box>

    )
}
