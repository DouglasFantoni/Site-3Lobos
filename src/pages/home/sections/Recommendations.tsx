import {Box} from "@mui/system";
import {Novel} from "../../../models/novel";
import NovelSlider from "../../../ui/components/NovelSlider/NovelSlider";

interface Props {
    novels: Array<Novel>

}

export default function Recommendations({novels}: Props) {

    return (
        <Box>
            {/*{recomendationsList.map((item, index) => (*/}
            {/*    // eslint-disable-next-line react/jsx-key*/}
            {/*    */}
            {/*))}*/}
            <NovelSlider novels={novels} size="sm"/>
        </Box>
    )
}
