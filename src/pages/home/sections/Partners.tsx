import {Box} from "@mui/system";
import Partner from "../../../models/partner";
import {ImageList, ImageListItem} from "@mui/material";
import Image from 'next/image'
import {isValidHttpUrl} from "../../../utils/others/localImageLoader";
import {getFile} from "../../../utils/helpers/files_helper";


// export const getServerSideProps: GetServerSideProps = async (context) => {
//
//
//     let promisePartners =  await appwrite.database.listDocuments(COLL_PARTNERS) as AppwriteResponse<Partner>;
//
//
//     return {
//         props: {
//             partnersList: promisePartners.documents,
//         },
//     }
// }


interface Props {
    partnersList: Array<Partner>,
}

export default function Partners({partnersList}: Props) {


    return (
        <Box>
            <ImageList sx={{  }} gap={15} cols={4} >
                {partnersList.map((item, index) => {

                    // TODO: Opção para aceitar links com url externa

                    const props = !isValidHttpUrl(item.logo) ? {
                        src:  getFile(item.logo)
                    } : {
                        src:  getFile(item.logo)
                    }

                    return (
                    <ImageListItem key={index} >
                        <a href={item.link} rel="noreferrer" target="_blank">
                            <Image
                                {...props}
                                alt={'Logo parceiro '+item.name}
                                title={item.name}
                                width={100}
                                height={100}
                            />
                        </a>

                    </ImageListItem>
                )})}
            </ImageList>
        </Box>
    )
}
