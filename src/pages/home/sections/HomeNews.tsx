import {Box, Grid, Paper} from "@mui/material";
import {styled} from "@mui/system";
import NoticeCard from "../../../ui/widgets/NoticeCard/NoticeCard";
import HomeNewsImage from "../../../ui/svgs/HomeNewsImage";
import {NewsFull} from "../../../models/news";


interface Props {
    news: Array<NewsFull>
}

export default function HomeNews({news}: Props) {

    return (
        <Box sx={{position: 'relative'}}>
            <Grid container spacing={2}>
                <Grid item  sm={5} >
                    <Box1>
                        <HomeNewsImage/>
                    </Box1>
                </Grid>
                <Grid item  sm={7} xs={12}>
                    {news.map((el, index) => (
                        <NoticeCard key={index} news={el}/>
                    ))}
                </Grid>
            </Grid>
            <Box sx={{display: 'flex', justifyContent: 'flex-end'}}>
                {/*<SimpleLink link="#">Ver todas</SimpleLink>*/}
            </Box>
        </Box>
    )
}


const Box1 = styled(Paper)(({theme}) => ({
    backgroundColor: theme.palette.primary.main,
    padding: '35px 45px 0 0',
    [theme.breakpoints.down('sm')]: {
        display: 'none'
    },
    // height: 300,
    position: 'relative',
}))
