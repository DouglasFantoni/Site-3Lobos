import {Box, styled} from "@mui/system";
import {MAIN_DARK, MAIN_LIGHT} from "../../../constants/ui/colors";
import CustomContainer from "../../../ui/components/Container/CustomContainer";
import Title from "../../../ui/components/Title/Title";
import {Typography} from "@mui/material";
import {SPACE_BETWEEN_SECTIONS} from "../../../constants/ui/variables";
import ImageBackground from "../../../ui/components/ImageBackground/ImageBackground";
import Image from "next/image";
import {SM} from "../../../constants/ui/breakpoints";
import Button from "@mui/material/Button";
import Link from 'next/link'
import localImageLoader from "../../../utils/others/localImageLoader";
import {SessionConsumer} from "../../../utils/providers/SessionProvider";


export default function BecomeAuthor() {

    const imageSize = 350;
    const bg = localImageLoader({src: '/images/backgrounds/confettis.svg',width: 0,quality: 0});

    return (
        <SessionConsumer>
            {({session}) =>
                <Box  sx={{backgroundColor: session.theme === 'Dark' ? MAIN_DARK : '#646ED2' }}>
                    <ImageBackground  src={bg}  >
                        <CustomContainer sx={{pb: SPACE_BETWEEN_SECTIONS}}>
                            <Box>
                                <Box sx={{display: 'flex', flexFlow: 'column', alignItems: 'center'}}>
                                    <Box sx={{mt: SPACE_BETWEEN_SECTIONS, mb: 4}}>
                                        <Image loader={localImageLoader} alt={'Pessoa escrevendo'} height={imageSize} width={imageSize} src={'/images/illustrations/woman_write.png'}/>
                                    </Box>

                                    <Title sx={{fontWeight: 'bold', mb: 1}}  type={'light'} badge={false} >Escreva sua própria história</Title>
                                    <Box sx={{maxWidth: SM, mb: 4}}>
                                        <Typography color={'primary'} variant='h2'>
                                            Escreva algumas páginas e envie elas para a gente. Quando aprovado você recebe um ótimo lugar para divulgar sua novel e vai sempre poder contar com o apoio da equipe 3Lobos.
                                        </Typography>
                                    </Box>
                                    <Link href={'editoria'} passHref>
                                        <Button  variant="outlinedWhite" >Quero ser um autor</Button>
                                    </Link>

                                </Box>

                            </Box>

                        </CustomContainer>
                    </ImageBackground>

                </Box>

            }
        </SessionConsumer>

    )
}


const PersonContainer = styled('div')(({theme}) => ({
    backgroundColor: '#fff',
    p: 2,

}));

const ImageBox = styled(Box)(({theme}) => ({
    backgroundColor: MAIN_LIGHT,
    overflow: 'hidden',
    width: '100%',
    height: '200px',
    borderRadius: '400px 0 0 0px',

}));
