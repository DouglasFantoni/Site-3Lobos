import {Box, Grid, TextField, Typography} from "@mui/material";
import Template from "../../ui/widgets/template/Template";
import Title from "../../ui/components/Title/Title";
import NovelSlider from "../../ui/components/NovelSlider/NovelSlider";
import {styled} from "@mui/system";
import HomeNews from "./sections/HomeNews";
import LastChapters from "./sections/LastChapters";
import WeDiscord from "./sections/WeDiscord";
import Partners from "./sections/Partners";
import Recommendations from "./sections/Recommendations";
import {FONT_SECONDARY} from "../../constants/ui/fonts";
import CustomContainer from "../../ui/components/Container/CustomContainer";
import {SPACE_BETWEEN_SECTIONS} from "../../constants/ui/variables";
import ImageBackground from "../../ui/components/ImageBackground/ImageBackground";
import BecomeAuthor from "./sections/BecomeAuthor";
import appwrite from "../../utils/providers/AppwriteProvider";
import {
    COLL_CHAPTERS,
    COLL_CONTENTS,
    COLL_NEWS,
    COLL_NOVELS,
    COLL_PARTNERS,
    COLL_VOLUMES
} from "../../constants/collectionsIds";
import {AppwriteResponse} from "../../types/appwritetypes";
import {Novel} from "../../models/novel";
import {GetServerSideProps} from 'next'
import News, {NewsFull} from "../../models/news"
import Content from "../../models/content";
import Chapter, {ChapterFull} from "../../models/chapter";
import Volume from "../../models/volume";
import Partner from "../../models/partner";
import {SessionConsumer} from "../../utils/providers/SessionProvider";
import React from "react";


export const getServerSideProps: GetServerSideProps = async (context) => {
    //
    // let promise2 = await new sdk.Database(client).createDocument(COLL_NOVELS,{
    //         "title": "Dark Net Galática",
    //         "author": "Sonic Nine Light Year (秒速九光年)",
    //         "synopsis": "<p>A Dark net é uma área ainda mais profunda da Deep Web, que não apenas proíbe qualquer tipo de indexação, mas também exige permissões especiais e ferramentas como proxies e autenticações simultaneas para acessá-la. A rede não respeita qualquer legislação ou código de ética, e assim possui tudo, literalmente tudo, que é defeso por lei.&nbsp;Drogas, escravos, uranio, armas de fogo, biológicas, químicas e nucleares, animais raros, testes com seres sentientes, assassinato, e assim por diante.</p><p>Durante o ano 2075, Han Lang fez seu login nessa rede macabra, para assim começar uma jornada em busca da fortuna.</p><p>Ocorre que o último prodígio da Terra, um esper de 5 estrelas, desapareceu sem deixar vestígios há quase uma década atrás, e por carecer de um protetor de nível avançado a Terra&nbsp;corre um sério risco de se tornar uma colônia de outro planeta mais forte, acabando de vez com o futuro dos terráqueos. Mas Han descobre que pode se tornar um esper com a ajuda da Dark Net.&nbsp;Só há um problema: o preço.</p>",
    //         "chaptersNumber": "353",
    //         "cover": "619437b826672",
    //         "score": "4.5",
    //         "idsGenres": [
    //             "61942a15e72f6", '619396096a3be', '619427e51b83f', '619396311993b',
    //         ],
    //         "idsTags": [
    //             '618181e00650b'
    //         ],
    //         "idNationality": "6193b6fc89bfd",
    //         "status": "Ativo",
    //         "slug": "dark-net-galatica",
    //         "moment": 1637070503,
    //         "views": 0,
    //         "alternativeTitle": 'Galactic Dark Net',
    //     }, ['*'], ['*']



    const NACIONALITY_BR = "618184fba1467";
    const TAG_RECOMENDADAS = "618181e00650b";

    let promise =  await appwrite.database.listDocuments(COLL_NOVELS, [],9,0,"views") as AppwriteResponse<Novel>;
    let promiseNews =  await appwrite.database.listDocuments(COLL_NEWS, [],3,0,"moment") as AppwriteResponse<News>;

    let newsFull: Array<NewsFull> = [];
    for (const news of promiseNews.documents){

        let data = await appwrite.database.getDocument(COLL_CONTENTS, news.idContent) as Content;

        newsFull.push(new NewsFull(
            news,
            data.content,
        ));
    }
    let promisePartners =  await appwrite.database.listDocuments(COLL_PARTNERS) as AppwriteResponse<Partner>;

    let promiseNovels3Lobos =  await appwrite.database.listDocuments(COLL_NOVELS, ["idNationality="+NACIONALITY_BR],12,0,undefined, undefined,undefined) as AppwriteResponse<Novel>;
    let promiseNovelsRecomendadas =  await appwrite.database.listDocuments(COLL_NOVELS, undefined,12,0,undefined, undefined,undefined,TAG_RECOMENDADAS) as AppwriteResponse<Novel>;
    let promiseLastChapters =  await appwrite.database.listDocuments(COLL_CHAPTERS, undefined,15,0,"moment", 'DESC', 'int') as AppwriteResponse<Chapter>;
    let lastChapters: Array<ChapterFull> = [];
    for (const chapter of promiseLastChapters.documents){

        let volumeData = await appwrite.database.getDocument(COLL_VOLUMES, chapter.idVolume) as Volume;
        let novelData = await appwrite.database.getDocument(COLL_NOVELS, volumeData.idNovel) as Novel;

        lastChapters.push(new ChapterFull(
            chapter,
            novelData,
        ));
    }

    // console.log('promisePartners',lastChapters)

    return {
        props: {
            novelsMostViwed: promise.documents,
            news: JSON.stringify(newsFull),
            novels3Lobos: promiseNovels3Lobos.documents,
            novelsRecomendadas: promiseNovelsRecomendadas.documents,
            lastChapters: JSON.stringify(lastChapters),
            partnersList: promisePartners.documents,
        },
    }
}


interface Props {
    novelsMostViwed: Array<Novel>,
    news: string,
    novels3Lobos: Array<Novel>,
    novelsRecomendadas: Array<Novel>,
    lastChapters: string,
    partnersList: Array<Partner>,
}

export default function Home({novelsMostViwed, news, novels3Lobos, novelsRecomendadas, lastChapters, partnersList}: Props){

    const newNews = JSON.parse(news) as Array<NewsFull>;

    // console.log("novelsRecomendadas",novelsRecomendadas)

    return (
        <Template title="3 Lobos">
            <Box sx={{position: 'relative'}}>
                <SessionConsumer>
                    {({session}) =>
                        <ImageBackground src={session.theme === 'Dark' ?  '': '/images/backgrounds/bgHomeTitle.svg' }>
                            <CustomContainer>
                                <Box sx={{pt: SPACE_BETWEEN_SECTIONS , mx: "auto"}}>
                                    <HomeTitle color={'text.primary'}>Webnovels de qualidade</HomeTitle>
                                </Box>
                            </CustomContainer>
                            <NovelSlider novels={novelsMostViwed} sx={{pt: SPACE_BETWEEN_SECTIONS}} size="md"/>
                        </ImageBackground>
                    }
                </SessionConsumer>

            </Box>

            <CustomContainer sx={{mt: SPACE_BETWEEN_SECTIONS}}>
                <Title>Notícias</Title>
                <HomeNews news={newNews}/>
            </CustomContainer>

            <CustomContainer sx={{zIndex: 2, mt: SPACE_BETWEEN_SECTIONS}}>

                <Title>Originais 3Lobos</Title>

            </CustomContainer>
            <NovelSlider novels={novels3Lobos} size="sm"/>

            <Box sx={{mt: SPACE_BETWEEN_SECTIONS,position: 'relative', overflow: 'hidden'}}>

                <SessionConsumer>
                    {({session}) =>
                        <ImageBackground src={session.theme === 'Dark' ? '' : '/images/backgrounds/bgLastChapters.svg'} sx={{ pt: 6, pb: 10,}}>
                            <CustomContainer sx={{}}>

                                <Box sx={{pt: 10,width: "max-content"}}>
                                    <Title>Últimos capítulos</Title>

                                </Box>
                                <LastChapters chapters={lastChapters}/>

                                <Box sx={{display: 'flex', justifyContent: 'flex-end', mt: 4}}>
                                    {/*<SimpleLink link="#">Ver todas</SimpleLink>*/}
                                </Box>

                            </CustomContainer>

                        </ImageBackground>
                    }
                </SessionConsumer>

            </Box>

            <Box sx={{}}>
                <BecomeAuthor/>
            </Box>
            <Box sx={{position: 'relative'}}>
                <CustomContainer sx={{mt: SPACE_BETWEEN_SECTIONS}}>
                    <Title>Recomendações</Title>
                </CustomContainer>

                <Recommendations novels={novelsRecomendadas } />
            </Box>

            <CustomContainer sx={{mt: 10}}>
                <Box sx={{mt: 9}}>
                    <Grid container spacing={2}>
                        <Grid item sx={{mt: SPACE_BETWEEN_SECTIONS}} xs={12} sm={6}>
                            <Title>Nosso discord</Title>

                            <WeDiscord/>
                        </Grid>
                        <Grid item sx={{mt: SPACE_BETWEEN_SECTIONS}} xs={12} sm={6}>
                            <Title>Parceiros</Title>

                            <Partners partnersList={partnersList}/>
                        </Grid>
                    </Grid>
                </Box>

            </CustomContainer>
        </Template>
    )
}

// const NewContainer = styled('div')`
//     background-image: url('/bgLastChapters.svg');
//     background-repeat: no-repeat;
//     background-size: cover;
// `;




const HomeTitle = styled(Typography)(({theme}) => ({
    fontFamily: FONT_SECONDARY,
    fontSize: 48,
    textAlign: 'center',
    fontWeight: 'bold',
}));

