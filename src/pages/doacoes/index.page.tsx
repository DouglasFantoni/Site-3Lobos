import { Card, CardContent, Grid, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import { Box } from "@mui/system";
import { GetServerSideProps } from "next";
import Image from "next/image";
import {
  SPACE_BETWEEN_SECTIONS,
  SPACE_INSIDE_SECTIONS,
} from "../../constants/ui/variables";
import BoxCentered from "../../ui/components/BoxCentered/BoxCentered";
import BtnBase from "../../ui/components/BtnBase/BtnBase";
import CustomContainer from "../../ui/components/Container/CustomContainer";
import Template from "../../ui/widgets/template/Template";
import { getFile } from "../../utils/helpers/files_helper";

const IMG_HEADER = "6217aed25637b";
const IMG_PAYPAL = "622fb70d59b6d";
const IMG_PADRIM = "622fb6fa12acf";
export const getServerSideProps: GetServerSideProps = async (context) => {
  return {
    props: {},
  };
};

export default function Doacoes() {
  return (
    <Template title={"Doações 3Lobos"}>
      <Box>
        <Box sx={{ background: "darkblue" }}>
          <Typography
            p={3}
            textAlign={"center"}
            variant={"h1"}
            color={"white"}
            mb={2}
            fontWeight={"bold"}
          >
            Doe para a 3Lobos
          </Typography>
        </Box>
        <CustomContainer sx={{}}>
          <Box>
            <Grid container sx={{ mt: SPACE_INSIDE_SECTIONS }} spacing={4}>
              <Grid item xs={12} sm={4}>
                <Box sx={{ borderRadius: 2, overflow: "hidden" }}>
                  <Image
                    src={getFile(IMG_HEADER)}
                    alt="Temos Padrin e Paypal!"
                    title={"Temos Padrin e Paypal!"}
                    width={800}
                    height={419}
                    layout={"responsive"}
                  />
                </Box>
              </Grid>
              <Grid item xs={12} sm={8}>
                <Typography>
                  Vocês já devem ter percebido que não estamos utilizando
                  anúncios. Parte porque é algo que nos limita, e também porque
                  é muito chato. Por isso decidimos ter zero anúncios no site,
                  melhorando a experiência de vocês. Pois bem, mas precisamos
                  manter o site de alguma forma.
                  <br />
                  <br />
                  Ultimamente viemos tirando do próprio bolso para manter o site
                  online. Só que sabemos que isso não dura pra sempre. Portanto,
                  decidimos utilizar o sistema de doações para tentar tornar o
                  site sustentável de alguma forma. Fazemos por hobby, e vocês
                  sabem disso. Valorizamos muito a qualidade e a experiência de
                  leitura.
                </Typography>
              </Grid>
            </Grid>

            <Grid container spacing={3}>
              <Grid item sx={{ mt: SPACE_BETWEEN_SECTIONS }} xs={12} sm={8}>
                <Typography>
                  Note que não venderemos capítulos. Não gostamos de lucrar em
                  cima do trabalho de outros, mesmo que nossas traduções adaptem
                  e consertem muitas coisas dos autores originais. Essa é uma
                  decisão da equipe.
                </Typography>

                <Typography>
                  Porém, vocês terão benefícios no Discord. Os benefícios variam
                  de cargos até uma sala exclusiva a doadores, onde eles poderão
                  tirar dúvidas e conversar prioritariamente com os tradutores
                  das novels. Poderá até escolher um emote para adesão no chat,
                  olha só. Sim, são coisas bobas, mas que demonstram nossa
                  gratidão.
                </Typography>
              </Grid>
              <Grid item sx={{ mt: SPACE_BETWEEN_SECTIONS }} xs={12} sm={4}>
                <Box sx={{ borderRadius: 2, overflow: "hidden" }}>
                  <Card>
                    <CardContent>
                      <Typography
                        p={3}
                        textAlign={"center"}
                        variant={"h1"}
                        mb={2}
                        fontWeight={"bold"}
                      >
                        Benefícios
                      </Typography>
                      <Box>
                        {/*<CustomBox>*/}
                        {
                          // Icone aquiii
                          // e texto do beneficio
                        }
                        {/*</CustomBox>*/}
                      </Box>
                    </CardContent>
                  </Card>
                </Box>
              </Grid>
            </Grid>

            <Grid container spacing={2} mt={SPACE_BETWEEN_SECTIONS} mb={16}>
              <Grid item xs={0} sm={2} />

              <Grid item xs={12} sm={4}>
                <Card variant={"outlined"} sx={{ padding: 3 }}>
                  <BoxCentered>
                    <Image
                      src={getFile(IMG_PADRIM)}
                      alt="Doe no Padrin!"
                      title={"Doe no Padrin!"}
                      width={170}
                      height={60}
                    />
                  </BoxCentered>

                  <Typography my={5}>
                    Aviso importante para o Padrim: Caso você apadrinhe, entre
                    em contato com um dos nossos ADMs no Discord (Gelo ou Kabum,
                    preferencialmente) para que possamos te identificar.
                  </Typography>

                  <BoxCentered>
                    <BtnBase
                      href={"https://www.padrim.com.br/3lobosoficial"}
                      sx={{ padding: 1 }}
                      variant={"contained"}
                      fullWidth
                      size={"large"}
                      color={"secondary"}
                    >
                      <Typography variant={"h3"} py={1} color={"primary"}>
                        DOAR NO PADRIM
                      </Typography>
                    </BtnBase>
                  </BoxCentered>
                </Card>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Card variant={"outlined"} sx={{ padding: 3 }}>
                  <BoxCentered>
                    <Image
                      src={getFile(IMG_PAYPAL)}
                      alt="Doe no Paypal!"
                      title={"Doe no Paypal!"}
                      width={170}
                      height={60}
                    />
                  </BoxCentered>
                  <Typography my={5}>
                    Aviso importante para o PayPal: Se você fizer alguma doação
                    por ele, não se esqueça de colocar seu nick do Discord na
                    parte de Nota. Assim poderemos te identificar.
                  </Typography>
                  <BoxCentered>
                    <BtnBase
                      href={
                        "https://www.paypal.com/donate/?hosted_button_id=BSUTJYLFNU4YS"
                      }
                      sx={{ padding: 1 }}
                      variant={"contained"}
                      fullWidth
                      size={"large"}
                      color={"secondary"}
                    >
                      <Typography variant={"h3"} py={1} color={"primary"}>
                        DOAR NO PAYPAL
                      </Typography>
                    </BtnBase>
                  </BoxCentered>
                </Card>
              </Grid>
              <Grid item xs={0} sm={2} />
            </Grid>
          </Box>
        </CustomContainer>
      </Box>
    </Template>
  );
}

const CustomBox = styled(Box)(({ theme }) => ({
  border: `2px solid ${theme.palette.background.paper}`,
}));
