import {Box, Chip, Grid, Paper, Typography} from "@mui/material";
import Template from "../../ui/widgets/template/Template";
import Title from "../../ui/components/Title/Title";
import {styled} from "@mui/system";
import CustomContainer from "../../ui/components/Container/CustomContainer";
import appwrite, {client} from "../../utils/providers/AppwriteProvider";
import SearchOffIcon from '@mui/icons-material/SearchOff';

import {COLL_CHAPTERS, COLL_GENRES, COLL_NOVELS, COLL_TAGS, COLL_VOLUMES} from "../../constants/collectionsIds";
import {AppwriteResponse} from "../../types/appwritetypes";
import {Novel} from "../../models/novel";
import {GetServerSideProps} from 'next'
import * as React from "react";
import {useState} from "react";
import Tag from "../../models/tag";
import NovelCardVertical from "../../ui/components/NovelCard/NovelCardVertical";
import Gender from "../../models/gender";
import sdk from "node-appwrite";
import Volume from "../../models/volume";
import Chapter from "../../models/chapter";


export const getServerSideProps: GetServerSideProps = async (context) => {

    try{
        let appWrite = new sdk.Database(client);

        // let novelPromise = await appWrite.listDocuments(COLL_NOVELS, undefined, 100, 0)  as AppwriteResponse<Novel>;
        let novelPromise = await appWrite.getDocument(COLL_NOVELS, '619427fe15306')  as Novel;
        let novel = novelPromise;
        // let offsetCaps = 0;
        // while (true){
        //     let chaptersPromise =  await appWrite.listDocuments(COLL_CHAPTERS, [`$id>=61ab751b9b10f`], 100, offsetCaps) as AppwriteResponse<Chapter>;
        //     chaptersPromise.documents.forEach(chapter => {
        //
        //         console.log(chapter.idVolume);
        //
        //             // appWrite.updateDocument(COLL_CHAPTERS, chapter.$id, {
        //             //     idVolume: `${novel.slug}-${chapter.slug}`
        //             // }).then().catch(console.error);
        //
        //         }
        //     )
        //     offsetCaps += chaptersPromise.documents.length;
        //
        //     if (offsetCaps === chaptersPromise.sum){
        //         break;
        //     }
        // }




        // novelPromise.documents.map(async (novel) => {
            let volumesPromise =  await appWrite.listDocuments(COLL_VOLUMES, [`idNovel=${novel.$id}`], 100,) as AppwriteResponse<Volume>;

            let offsetCaps = 0;
            for (const volume of volumesPromise.documents) {

                while (true){
                    let chaptersPromise =  await appWrite.listDocuments(COLL_CHAPTERS, [`idVolume=${volume.$id}`], 100, offsetCaps, 'slug', undefined, 'int') as AppwriteResponse<Chapter>;

                    chaptersPromise.documents.forEach(chapter => {

                        let ordem = chapter.slug.replace(/[^0-9]/g,'');

                        // console.log(chapter.slug, chapter.ordem, )

                        // appWrite.updateDocument(COLL_CHAPTERS, chapter.$id, {
                        //     ordem: ordem,
                        // }).then().catch(console.error);
                    });

                    offsetCaps += chaptersPromise.documents.length;
                    console.log(chaptersPromise.sum, offsetCaps, chaptersPromise.documents.length);
                    if (offsetCaps === chaptersPromise.sum){
                        break;
                    }
                }
            }
        // })
    } catch (error){
        console.error(error);
    }

    let tagsPromise =  await appwrite.database.listDocuments(COLL_TAGS, ) as AppwriteResponse<Tag>;
    let gendersPromise =  await appwrite.database.listDocuments(COLL_GENRES, ) as AppwriteResponse<Gender>;

    return {
        props: {
            tags: tagsPromise.documents,
            genders: gendersPromise.documents,
        },
    }
}

interface Props {
    novels: Array<Novel>,
    tags: Array<Tag>,
    genders: Array<Gender>,
}

class TagsFilter extends Tag {
    constructor(tag:Tag, public filtered = false) {
        super(tag.$id, tag.title);
    }
}
class GenderFilter extends Gender {
    constructor(gender: Gender, public filtered = false) {
        super(gender.$id, gender.title);
    }
}

export default function Novels({novels, tags, genders}: Props){

    return (
        <>
        </>
    )
}

