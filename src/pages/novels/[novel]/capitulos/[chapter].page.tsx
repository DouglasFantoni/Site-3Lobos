import Template from "../../../../ui/widgets/template/Template";
import {Container, Divider, Typography} from "@mui/material";
import Button from '@mui/material/Button';
import {Box, useTheme} from "@mui/system";
import CustomContainer from "../../../../ui/components/Container/CustomContainer";
import {FONT_SECONDARY} from "../../../../constants/ui/fonts";
import DisqusComments from "../../../../ui/components/DisqusComments/DisqusComments";
import Chapter from "../../../../models/chapter";
import BtnUp from "../../../../ui/components/BtnUp/BtnUp";
import {CONTAINER_SMALL_WIDTH} from "../../../../constants/ui/variables";
import Link from 'next/link'
import {ArrowBack, ArrowForward} from "@mui/icons-material";
import {GetServerSideProps} from "next";
import appwrite, {client} from "../../../../utils/providers/AppwriteProvider";
import {COLL_CHAPTERS, COLL_CONTENTS, COLL_NOVELS, COLL_VOLUMES} from "../../../../constants/collectionsIds";
import {AppwriteResponse} from "../../../../types/appwritetypes";
import {Novel} from "../../../../models/novel";
import Content from "../../../../models/content";
import * as React from "react";
import CustomBreadcumb from "../../../../ui/widgets/Breadcumb/Breadcumb";
import sdk from "node-appwrite";
import {SessionConsumer} from "../../../../utils/providers/SessionProvider";
import useWindowSize from "../../../../utils/helpers/useWindowSize";
import {MD} from "../../../../constants/ui/breakpoints";


export const getServerSideProps: GetServerSideProps = async (context) => {

    // @ts-ignore
    const chapterName = context.params.chapter ;

    // @ts-ignore
    const novelName = context.params.novel;

    // let novelPromise =  await appwrite.database.listDocuments(COLL_CHAPTERS, [`slug=${chapterName}`]) as AppwriteResponse<Novel>;
    let novelPromise =  await appwrite.database.listDocuments(COLL_NOVELS, [`slug=${novelName}`], 1) as AppwriteResponse<Novel>;
    const novel = novelPromise.documents[0] as Novel;

    let chapterPromise =  await appwrite.database.listDocuments(COLL_CHAPTERS, [`slug=${chapterName}`], 1) as AppwriteResponse<Chapter>;
    const chapter = chapterPromise.documents[0] as Chapter;

    let content =  await appwrite.database.getDocument(COLL_CONTENTS,chapter.idContent ) as AppwriteResponse<Content>;

    const ordem = chapter.ordem;
    let nextChapter =  await appwrite.database.listDocuments(COLL_CHAPTERS, [`ordem=${parseInt(`${chapter.ordem}`) + 1}`, `idVolume=${chapter.idVolume}`], 1) as AppwriteResponse<Chapter>;
    let prevChapter =  await appwrite.database.listDocuments(COLL_CHAPTERS, [`ordem=${parseInt(`${chapter.ordem}`) - 1}`, `idVolume=${chapter.idVolume}`], 1) as AppwriteResponse<Chapter>;

    new sdk.Database(client).updateDocument(COLL_CHAPTERS, chapter.$id, {
        'views': chapter.views + 1,
    }).then();

    // console.log(chapter, nextChapter, prevChapter, chapter.ordem , );

    return {
        props: {
            // novel: novel,
            // volumes: JSON.stringify(volumesList),
            chapter: chapter,
            nextChapter: nextChapter.documents[0] ?? null,
            prevChapter: prevChapter.documents[0]  ?? null,
            novel: novel,
            content: content,
            // lastChapter: lastChapter,
        },
        // notFound: !(typeof chapter === 'object')
    }
}


interface Props {
    // novel: Novel,
    chapter: Chapter,
    nextChapter: Chapter | null,
    prevChapter: Chapter | null,
    content: Content,
    novel: Novel,
}


export default  function Capitulo({ chapter, content, novel, nextChapter, prevChapter}: Props) {


    const {isBreakpoint} = useWindowSize();



    const NextButton = nextChapter ? (
        <Link href={`/novels/${novel.slug}/capitulos/${nextChapter.slug}`} passHref>
            <Button  variant="outlined" endIcon={<ArrowForward/>}>
                Próximo
            </Button>
        </Link>
    ) : <div/>;

    const PrevButton = prevChapter ? (
        <Link href={`/novels/${novel.slug}/capitulos/${prevChapter.slug}`} passHref>
            <Button  variant="outlined" startIcon={<ArrowBack/>}>
                Anterior
            </Button>
        </Link>
    )  : <div/>;

    const links = {
        'Novels': '/novels',
        [`${novel.title}`]: `/novels/${novel.slug}`,
        [`${chapter.title}`]: `/novels/${chapter.slug}`,
    };

    const headerControls = isBreakpoint(MD) ?
        (
            <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                {PrevButton}
                <Typography color="textPrimary" sx={{mb: 4}} variant="h1" align="center">{chapter.title}</Typography>
                {NextButton}
            </Box>
        ) : (
            <Box sx={{display: 'flex', flexFlow: 'column', alignItems: 'center'}}>
                <Box sx={{display: 'flex', justifyContent: 'space-between', width: '100%'}}>
                    {PrevButton}
                    {NextButton}
                </Box>

                <Box my={4} sx={{display: 'flex', justifyContent: 'center'}}>
                    <Typography color="textPrimary"  variant="h1" align="center">{chapter.title}</Typography>
                </Box>
            </Box>
        )


    return (
        <Template title={chapter.title}>

            <CustomContainer >
                <Box mt={6}>
                    <CustomBreadcumb links={links}/>
                </Box>

                <Box sx={{mt:8}}>

                    {headerControls}

                    <Box >



                        {/*<Box sx={{ml:1}}>*/}
                        {/*    <Link  href={`novels/${novel.slug}`} passHref>*/}
                        {/*        <Button sx={{font}}  variant="outlined" endIcon={}>*/}
                        {/*        </Button>*/}
                        {/*    </Link>*/}
                        {/*</Box>*/}

                    </Box>
                    <Typography color="textPrimary" variant="h3">Tradutor: {chapter.translator} | Revisor: {chapter.reviewer}</Typography>

                    <Divider sx={{mb: 5}}/>
                    <Container sx={{maxWidth: `${CONTAINER_SMALL_WIDTH}px`}}>
                       <SessionConsumer>
                           {({session}) => {

                               return (
                                   <Box sx={{
                                       '*': {
                                           fontFamily: FONT_SECONDARY,
                                           color: 'text.primary',
                                           fontSize: session.fontsize,
                                           lineHeight: '1.8',
                                           whiteSpace: 'pre-line',
                                       },
                                       'img': {
                                           margin: '16px auto',
                                           display: 'block',
                                       }
                                   }}>
                                       <div dangerouslySetInnerHTML={{ __html: content.content}} />
                                   </Box>
                               )
                           }

                           }
                       </SessionConsumer>

                    </Container>
                    <Divider sx={{my: 5}}/>

                    <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center'}}>
                        {PrevButton}
                        {NextButton}
                    </Box>

                    <Box sx={{mt: 8}}>
                        <DisqusComments id={chapter.slug} title={novel.title}/>
                    </Box>
                </Box>

            </CustomContainer>
            <BtnUp/>

        </Template>
    )
}
//
// const TextWarpper = styled(Box)(({ theme }) => ({
//
// }));

