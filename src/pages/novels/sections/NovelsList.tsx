import {Novel} from "../../../models/novel";
import NovelCardVertical from "../../../ui/components/NovelCard/NovelCardVertical";


interface Props {
    novels: Array<Novel>
}


export default function NovelsList({novels}: Props) {

    return (
        <>
            {novels.map(el => (
                <NovelCardVertical key={el["$id"]} size={"sm"} novel={el}/>
            ))}
        </>
    )
}
