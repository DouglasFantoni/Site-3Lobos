import {Chip, Grid, Typography} from "@mui/material";
import NovelCardVertical from "../../../../ui/components/NovelCard/NovelCardVertical";
import {Box, styled} from "@mui/system";
import {Novel} from "../../../../models/novel";
import {DESTACH, WHITE} from "../../../../constants/ui/colors";
import Gender from "../../../../models/gender";


interface Props {
    genres: Array<Gender>,
    novel: Novel
}

export default function NovelData({genres, novel}: Props) {

    return (
        <Grid container spacing={5} >
            <Grid item xs={12} md={4} sx={{}}>
                <NovelCardVertical size={"md"} novel={novel} />
            </Grid>
            <Grid item xs={12} md={8}>
                <Box sx={{
                    m:1,
                }}>
                    <Typography color={'secondary'} sx={{ fontWeight: 'bold', }} variant={'h1'}>{novel.title}</Typography>
                    <Box sx={{my: 2}}>
                        {
                            genres.map(genres =>  (<Chip
                                        clickable={false}
                                        sx={{
                                            fontWeight: "bold",
                                            backgroundColor: DESTACH,
                                            fontSize: 14,
                                            color: WHITE,
                                            borderColor: WHITE,
                                            marginRight: 1
                                        }}
                                        key={genres.$id}
                                        label={genres.title}
                                        variant={"outlined"}
                                    />)
                            )
                        }
                    </Box>
                    <CustomText color={'text.primary'} variant={'h3'} >
                        <div dangerouslySetInnerHTML={{__html: novel.synopsis}}/>
                    </CustomText>
                </Box>
            </Grid>
        </Grid>
    )
}


const CustomText = styled(Typography)(({theme}) => ({
    '&,*': {
        // @ts-ignore
        ...theme.typography.h3,
    },
}));
