import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Button,
    Chip,
    Divider,
    Grid,
    Paper,
    Typography
} from "@mui/material";
import {Box, styled} from "@mui/system";
import ChapterDetail from "../../../ui/components/Chapter/Chapter";
import {ExpandMore, Favorite, Star, Visibility} from "@mui/icons-material";
import {Novel} from "../../../models/novel";
import {VolumesList} from "../../../models/volume";
import Chapter from "../../../models/chapter";
import {ST_ATIVO, ST_FINALIZADO, ST_PAUSADO} from "../../../types/novelTypes";
import BtnAction from "../../../ui/components/BtnAction/BtnAction";
import appwrite from "../../../utils/providers/AppwriteProvider";
import {COLL_CHAPTERS} from "../../../constants/collectionsIds";
import {AppwriteResponse} from "../../../types/appwritetypes";
import React, {useState} from "react";
import {CHAPTERS_LOAD_NUMBER} from "../../../constants/configs";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

interface Props {
    novel: Novel,
    total: number,
    volumes: Array<VolumesList>,
    firstChapter: Chapter,
}


export default function NovelInfo({novel, total, volumes, firstChapter}: Props) {

    const [volList, setVolList] = useState(volumes);

    let accordions: any = {};

    Object.values(volumes).forEach((vol, key) => {
        accordions[vol.volume.$id]  = (key === 0);
    })

    const [expanded, setExpanded] = useState<{[index: string ]: boolean}>(accordions);

    const handleChange = (id: string) => {
        // console.log(!expanded[id],id);

            let itens = {
                ...expanded,

            }
            itens[id] = !expanded[id]

            setExpanded(itens);
        };

    const loadChapters = (vol: VolumesList, offset: number) => {
        appwrite.database.listDocuments(COLL_CHAPTERS, [`idVolume=${vol.volume.$id}`], CHAPTERS_LOAD_NUMBER, offset,  'ordem', 'DESC', 'int')

    .then((response: AppwriteResponse<Chapter> | unknown)  => {
                    if (response){
                        // console.log(response)
                        let volsList = [
                            // @ts-ignore
                            ...volList,
                        ];
                        // @ts-ignore
                        volsList[volList.indexOf(vol)].chapters.push(...response.documents)

                        setVolList(volsList);
                        // @ts-ignore
                }
            })
    }

    // @ts-ignore
    return (
        <Grid   container spacing={2} flexWrap={'wrap-reverse'}>
            <Grid  item xs={12} md={7}  >
                <Paper sx={{p:2}}>
                    {

                        volList.map((vol, key) => {
                            // console.log('vol', vol,expanded[vol.volume.$id]);

                            const title =  (
                                <Box >
                                    <Typography sx={{textAlign: "left"}} fontWeight={'bold'} my={2} variant={"h2"}>{vol.volume.title}</Typography>
                                    {/*<Divider/>*/}
                                </Box>
                            );

                            return (
                                <Accordion elevation={0} disableGutters key={key} expanded={expanded[vol.volume.$id]} onChange={() => handleChange(vol.volume.$id)}>
                                    <AccordionSummary
                                        expandIcon={<ExpandMoreIcon />}
                                    >
                                        {title}
                                    </AccordionSummary>
                                    <AccordionDetails>
                                        {
                                            vol.chapters.map((chapter, key2) => {

                                                return (
                                                    <Box key={key2} >
                                                        <ChapterDetail novelSlug={novel.slug} chapter={chapter}/>
                                                        {vol.chapters.length -1 === key2 ?   null : <Divider/>}
                                                    </Box>
                                                )
                                            })
                                        }
                                        <Box sx={{display: 'flex', justifyContent: 'center'}} mt={4}>

                                            <Button variant={'outlined'} onClick={() => loadChapters(vol, vol.chapters.length)} startIcon={<ExpandMore/>} endIcon={<ExpandMore/>} sx={{margin: 'auto'}} >Carregar mais</Button>
                                        </Box>

                                    </AccordionDetails>
                                </Accordion>
                            )
                        })

                    }
                </Paper>
            </Grid>
            <Grid item xs={12} md={5}>
                <Paper sx={{
                    p:0
                }}>
                    <Box sx={{display: "fex", p:2}}>
                        <Box sx={{flexGrow: 1, display: "flex", alignItems: "center", flexFlow: "column"}}>
                            <Visibility/>
                            <Typography sx={{pt:1}} variant={"h3"}>{novel.views}</Typography>
                        </Box>
                        <Box sx={{flexGrow: 1, display: "flex", justifyContent: "center"}}>
                            <MyDivider/>
                        </Box>
                        <Box sx={{flexGrow: 1, display: "flex", alignItems: "center", flexFlow: "column"}}>
                            <Star/>
                            <Typography sx={{pt:1}} variant={"h3"}>{novel.score}</Typography>
                        </Box>
                        <Box sx={{flexGrow: 1, display: 'flex', justifyContent: "center"}}>
                            <MyDivider/>
                        </Box>
                        <Box  sx={{ flexGrow: 1, display: "flex", alignItems: "center", flexFlow: "column"}}>
                            <Favorite/>
                            <Typography sx={{pt:1}} variant={"h3"}>122</Typography>
                        </Box>
                    </Box>
                    <Box sx={{m:2}}>
                        <Box>
                            <Typography color={'text.primary'}  variant={"h3"} >Autor</Typography>
                            <Typography color={'text.primary'}  variant={"h2"} >{novel.author}</Typography>
                        </Box>
                        <Box sx={{py: 1}}>
                            <Typography color={'text.primary'}  variant={"h3"} >Tadutor</Typography>
                            <Typography  color={'text.primary'}  variant={"h2"} >{firstChapter.translator.length ? firstChapter.translator : (firstChapter.reviewer.length ? firstChapter.reviewer : 'Não informado')}</Typography>
                        </Box>
                        <Box sx={{pt: 1}}>
                            <Typography color={'text.primary'}  variant={"h3"} >Revisor</Typography>
                            <Typography color={'text.primary'}  variant={"h2"} >{firstChapter.reviewer.length ? firstChapter.reviewer : (firstChapter.translator.length ? firstChapter.translator : 'Não informado')}</Typography>
                        </Box>
                    </Box>
                    <Box sx={{m:2}}>
                        <InfoBox sx={{ display: 'flex', alignItems: 'center'}}>
                            <Typography color={'text.primary'} sx={{fontWeight: 'bold', mr: 2}} variant={"h2"}>Status: </Typography>
                            {/**@ts-ignore **/ }
                            { chipState[novel.status] ?? chipState[ST_ATIVO]}

                        </InfoBox>
                        <InfoBox>
                            <Typography color={'text.primary'} sx={{fontWeight: 'bold', mr: 2}} variant={"h2"}>StatusRaw: { novel.status ?? ST_ATIVO}</Typography>
                        </InfoBox>
                        <InfoBox>
                            <Typography color={'text.primary'} sx={{ fontWeight: 'bold', mr: 2}} variant={"h2"}>N° de capítulos: {total}</Typography>
                        </InfoBox>
                        <InfoBox>
                            <Typography color={'text.primary'} sx={{ fontWeight: 'bold', mr: 2}} variant={"h2"}>Nomes alternativos: {novel.alternativeTitle}</Typography>
                        </InfoBox>
                        <InfoBox>
                        </InfoBox>
                    </Box>
                    <Box sx={{m:2, pb:2, display: 'flex', justifyContent: 'center'}}>
                        <BtnAction  text={'Ler primeiro capítulo'} link={`/novels/${novel.slug}/capitulos/${firstChapter.slug}`} />
                    </Box>
                </Paper>
            </Grid>
        </Grid>

    )
}
const InfoBox = styled(Box)(({theme}) => ({
    display: 'flex',
    alignItems: 'center',
    margin: `${theme.spacing(2)} 0`,
}));

const chipProps = {
    color: 'white',
    fontWeight: 'bold',
};

const chipState = {
    [ST_ATIVO]:    <Chip
        label={ST_ATIVO}
        sx={{backgroundColor: 'green', ...chipProps}}
    />,
    [ST_PAUSADO]:    <Chip
        label={ST_PAUSADO}
        sx={{backgroundColor: 'red', ...chipProps}}
    />,
    [ST_FINALIZADO]:    <Chip
        label={ST_FINALIZADO}
        sx={{backgroundColor: 'yellow', ...chipProps}}
    />,
};


const MyDivider = styled('div')(({theme}) => ({

    height: "auto",
    width: 1,
    backgroundColor: "rgba(0, 0, 0, 0.12)",
    margin: "0 auto",

}));
