import {Box, Chip, Grid, Paper, Typography} from "@mui/material";
import Template from "../../ui/widgets/template/Template";
import Title from "../../ui/components/Title/Title";
import {styled} from "@mui/system";
import CustomContainer from "../../ui/components/Container/CustomContainer";
import appwrite from "../../utils/providers/AppwriteProvider";
import SearchOffIcon from '@mui/icons-material/SearchOff';

import {COLL_GENRES, COLL_NOVELS, COLL_TAGS} from "../../constants/collectionsIds";
import {AppwriteResponse} from "../../types/appwritetypes";
import {Novel} from "../../models/novel";
import {GetServerSideProps} from 'next'
import * as React from "react";
import {useState} from "react";
import Tag from "../../models/tag";
import NovelCardVertical from "../../ui/components/NovelCard/NovelCardVertical";
import Gender from "../../models/gender";


export const getServerSideProps: GetServerSideProps = async (context) => {

    let promise =  await appwrite.database.listDocuments(COLL_NOVELS, [],9,0,"title") as AppwriteResponse<Novel>;
    let tagsPromise =  await appwrite.database.listDocuments(COLL_TAGS, ) as AppwriteResponse<Tag>;
    let gendersPromise =  await appwrite.database.listDocuments(COLL_GENRES, ) as AppwriteResponse<Gender>;

    return {
        props: {
            novels: promise.documents,
            tags: tagsPromise.documents,
            genders: gendersPromise.documents,
        },
    }
}

interface Props {
    novels: Array<Novel>,
    tags: Array<Tag>,
    genders: Array<Gender>,
}

class TagsFilter extends Tag {
    constructor(tag:Tag, public filtered = false) {
        super(tag.$id, tag.title);
    }
}
class GenderFilter extends Gender {
    constructor(gender: Gender, public filtered = false) {
        super(gender.$id, gender.title);
    }
}

export default function Novels({novels, tags, genders}: Props){

    const ListItem = styled('li')(({ theme }) => ({
        margin: theme.spacing(0.5),
    }));

    const [novelsState, setNovelsState] = useState(novels as Array<Novel>);
    const [tagsList, setTagsList] = useState(tags as Array<TagsFilter>);
    const [gendersList, setGenderList] = useState(genders as Array<GenderFilter>);
    const links = {
        'Novels': '/novels',
    };
    const updateNovels = () => {

        let newNovels: Array<Novel> = [...novels];

        for (const tag of tagsList){
            if (tag.filtered){
                for (const novel of newNovels){
                    if (!(newNovels.indexOf(novel) < 0 ) &&  !novel.idsTags.includes(tag.$id) ){
                        newNovels.splice(newNovels.indexOf(novel), 1);

                    }
                }
            }
        }

        for (const gender of gendersList){
            if (gender.filtered){
                for (const novel of newNovels){
                    if (!(newNovels.indexOf(novel) < 0 ) && !novel.idsGenres.includes(gender.$id)  ){
                        newNovels.splice(newNovels.indexOf(novel), 1);
                    }
                }
            }
        }

        setNovelsState(newNovels);
    }

    const handleDeleteTag = (tagToRemove: TagsFilter) => () => {

        let copy = [...tagsList]
        copy[tagsList.indexOf(tagToRemove)].filtered = false;

        setTagsList(copy);

        updateNovels();
    };

    const handleDeleteGender = (genderToRemove: GenderFilter) => () => {

        let copy = [...gendersList]
        copy[gendersList.indexOf(genderToRemove)].filtered = false;

        setGenderList(copy);

        updateNovels();
    };


    const handleClick = (tagToFilter: TagsFilter) => {

        let copy = [...tagsList]
        copy[tagsList.indexOf(tagToFilter)].filtered = true;

        setTagsList(copy)
        updateNovels();
    };

    const handleClickGender = (genderToFilter: GenderFilter) => {

        let copy = [...gendersList]
        copy[gendersList.indexOf(genderToFilter)].filtered = true;

        setGenderList(copy)
        updateNovels();
    };

    // console.log(novelsState.length,novelsState)

    const NovelsBox =  (novelsState.length ?
        <Grid  ml={-9} mt={-1} flexWrap={'wrap'} container spacing={7}>
            {novelsState.map(el => (
                <Grid key={el["$id"]} sx={{}} item sm={4} md={3} xs={12} >
                    <NovelCardVertical  size={"sm"} novel={el}/>
                </Grid>
            ))}
        </Grid>
         : <NothingBox/>);


    return (
        <Template title="3 Lobos - Novels">
            <CustomContainer sx={{mt: 10}}>
                <Title>Novels</Title>
                <Box>
                    <Paper

                    sx={{p:4}}
                    >
                        <Typography sx={{mb:2}} variant={"h2"}>Filtre por Tags</Typography>
                        <Box component="ul"   sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            listStyle: 'none',
                            p: 0,
                            m: 0,
                        }}>
                            {tagsList.map((tag) => {
                                let props = (tag.filtered) ? {
                                    onDelete: handleDeleteTag(tag)
                                } : {
                                    onClick: () => handleClick(tag)
                                }
                                // @ts-ignore
                                return (
                                    <ListItem key={tag['$id']}>
                                        <Chip
                                            label={tag.title}
                                            {...props}
                                            variant={"outlined"}
                                        />
                                    </ListItem>
                                );
                            })}
                        </Box>

                        <Typography sx={{my:2}} variant={"h2"}>Filtre por Gêneros</Typography>
                        <Box component="ul"   sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            listStyle: 'none',
                            p: 0,
                            m: 0,
                        }}>
                            {gendersList.map((gender) => {
                                let props = (gender.filtered) ? {
                                    onDelete: handleDeleteGender(gender)
                                } : {
                                    onClick: () => handleClickGender(gender)
                                }
                                // @ts-ignore
                                return (
                                    <ListItem key={gender['$id']}>
                                        <Chip
                                            label={gender.title}
                                            {...props}
                                            variant={"outlined"}
                                        />
                                    </ListItem>
                                );
                            })}
                        </Box>
                    </Paper>
                </Box>
                {NovelsBox}

            </CustomContainer>
        </Template>
    )
}

const NothingBox = () => (
    <Box display={'flex'}  sx={{mt: 10, mb: 15, justifyContent: 'center', alignItems: 'center', flexFlow: 'column'}}>
        <SearchOffIcon sx={{mb:4, fontSize: '80px'}}/>
        <Typography variant={'h2'}>Nenhuma novel encontrada. Verifique os filtros</Typography>
    </Box>
)
