import Template from "../../ui/widgets/template/Template";
import {GetServerSideProps} from "next";
import appwrite from "../../utils/providers/AppwriteProvider";
import {COLL_CHAPTERS, COLL_GENRES, COLL_NOVELS, COLL_VOLUMES} from "../../constants/collectionsIds";
import {AppwriteResponse} from "../../types/appwritetypes";
import CustomContainer from "../../ui/components/Container/CustomContainer";
import {Novel} from "../../models/novel";
import {Box} from "@mui/system";
import Chapter from "../../models/chapter";
import Volume, {VolumesList} from "../../models/volume";
import NovelData from "./sections/NovelData/NovelData";
import NovelInfo from "./sections/NovelInfo";
import CustomBreadcumb from "../../ui/widgets/Breadcumb/Breadcumb";
import Gender from "../../models/gender";


export const getServerSideProps: GetServerSideProps = async (context) => {

    // @ts-ignore
    const novelName = context.params.novel;


    let novelPromise =  await appwrite.database.listDocuments(COLL_NOVELS, [`slug=${novelName}`]) as AppwriteResponse<Novel>;
    const novel = novelPromise.documents[0] as Novel;

    let strGenres: Array<Gender> = [];
    for(let genre of novel.idsGenres){
        strGenres.push(await appwrite.database.getDocument(COLL_GENRES, genre) as Gender);
    }

    let volumesPromise =  await appwrite.database.listDocuments(COLL_VOLUMES, [`idNovel=${novel.$id}`],undefined,undefined, '$id', 'DESC') as AppwriteResponse<Volume>;

    let volumesList: Array<VolumesList> = [];
    let total = 0;
    for (const volume of volumesPromise.documents){

            let chapterPromise =  await appwrite.database.listDocuments(COLL_CHAPTERS, [`idVolume=${volume.$id}`], 15, undefined, 'ordem', 'DESC', 'int') as AppwriteResponse<Chapter>;
        total = chapterPromise.sum;
            volumesList.push(new VolumesList(volume, chapterPromise.documents));

    }

    let firstChapter =  await appwrite.database.listDocuments(COLL_CHAPTERS, [`idVolume=${volumesList[volumesList.length - 1].volume.$id}`], 1, undefined, 'ordem', 'ASC') as AppwriteResponse<Chapter> ;
    // console.log('firstChapter',firstChapter)

    return {
        props: {
            novel: novel,
            total: total,
            volumes: JSON.stringify(volumesList),
            genres: strGenres,
            firstChapter: firstChapter.documents[0],
        },
        // notFound: !(typeof novel === 'object')
    }
}



interface Props {
    novel: Novel,
    total: number,
    volumes: string,
    genres: Array<Gender>,
    firstChapter: Chapter
}

export default function NovelDetails({novel, total, volumes, genres,firstChapter}: Props) {

    const newVolumes = JSON.parse(volumes) as Array<VolumesList>;

    const links = {
        'Novels': '/novels',
        [`${novel.title}`]: `/novels/${novel.slug}`,
    };


    return (
        <Template title={`3 Lobos - ${novel.title}`}>
            <Box sx={{backgroundColor: 'blue.dark', pb: 5}}>

                <CustomContainer >
                    <Box pt={6}>
                        <CustomBreadcumb  links={links}/>
                    </Box>
                </CustomContainer>

                <CustomContainer sx={{pb: 10, pt: 5}}>
                    <NovelData genres={genres} novel={novel}/>
                </CustomContainer>
            </Box>
            <CustomContainer sx={{mt: -13}}>
                <NovelInfo novel={novel} total={total} volumes={newVolumes} firstChapter={firstChapter}/>
            </CustomContainer>
        </Template>
    )
}


