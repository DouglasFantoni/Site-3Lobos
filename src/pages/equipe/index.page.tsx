import Template from "../../ui/widgets/template/Template";
import {Box, styled} from "@mui/system";
import CustomContainer from "../../ui/components/Container/CustomContainer";
import {Avatar, Divider, Grid, Paper, Typography} from "@mui/material";
import users from './users.json';
import TeamList from "./sections/TeamList";
import {GetServerSideProps} from "next";
import useSWR from 'swr'
import appwrite, {client} from "../../utils/providers/AppwriteProvider";
import {COLL_NOVELS} from "../../constants/collectionsIds";
import {AppwriteResponse} from "../../types/appwritetypes";
import {Novel} from "../../models/novel";
import User from "../../models/user";
import sdk from "node-appwrite";
import GravatarApi, {BASE_URL_GRAVATAR} from "../../utils/requests/gravatarApi";
import createUserHash from "../../utils/helpers/gravatarHelper";
import axios from "axios";
import {useEffect, useMemo} from "react";
import Recruitment from "./sections/Recruitment";
import {LIGHT} from "../../constants/ui/themes";
import {BLACK, FULL_BLACK, WHITE} from "../../constants/ui/colors";

const URL = `${BASE_URL_GRAVATAR}${createUserHash('dfantoni2@gmail.com')}.json`;

export const getServerSideProps: GetServerSideProps = async (context) => {

    // let promise =  await appwrite.database.listDocuments(COLL_NOVELS, [],9,0,"views") as AppwriteResponse<Novel>;
    const NACIONALITY_BR = "618184fba1467";

    // let users = new sdk.Users(client);
    let users = new sdk.Users(client);
    // let promise = users.updatePrefs('[USER_ID]', {});
    let promiseNovels3Lobos =  await appwrite.database.listDocuments(COLL_NOVELS, ["idNationality="+NACIONALITY_BR],12,0,undefined, undefined,undefined) as AppwriteResponse<Novel>;


    // let promise = users.updatePrefs('617bf706c833c', {
    //     theme: 'light',
    //     avatar: 'https://pt.gravatar.com/userimage/110214935/497c6deeb14f5394122b7009000cb78c.jpg?size=200',
    //     occupation: 'Tradutor de LAB',
    //     description: 'Minha autobiografia aquiiii',
    //     social: [
    //         'https://facebook.com.br'
    //     ]
    // });

    const peoples = await users.list() as {users: User[]}
    console.log('URL', URL);

    // const peopleInfo = await GravatarApi.get(URL) as GravatarResponse;


return {
    props: {
        // novelsMostViwed: promise.documents,
            peoples: [...peoples.users, ...peoples.users, ...peoples.users,...peoples.users],
            // peopleInfo: peopleInfo.entry,
            novelsToTraduct: promiseNovels3Lobos.documents,
            novelsToReview: promiseNovels3Lobos.documents,

        }
    }
}

interface Props {
    peoples: Array<User>,
    novelsToTraduct: Array<Novel>,
    novelsToReview: Array<Novel>,
    // peopleInfo: any,
}


export default function Equipe(props: Props) {

    const IMAGE_SIZE = 100
    // const { data, error } = useSWR(URL, GravatarApi.get, {fallbackData: props.peopleInfo[0]})
    console.log(props);

    return (
        <Template title={'Equipe 3Lobos'}>

            <Box mt={8}>
                <CustomContainer sx={{mb: 8}}>
                    <Typography variant={'h3'}>
                        A 3Lobos é um site de publicação de webnovels  traduzidas e de webnovels brasileiras com qualidade!
                        <br/>
                        <br/>
                        Os fundadores da 3Lobos são 4 professores brasileiros apaixonados por livros e que, sempre que possível, também gostam de contar a sua própria história. Em meio a tantos movimentos de tradução de webnovels chinesas e coreanas para o português, a 3Lobos visa a tradução dessas histórias que, de certa forma, possuem
                        <br/>
                        <br/>
                        {/*Você pode perceber que usamos expressões e pontuações conforme as normas literárias em nossas traduções. Além disso, nossa equipe inicial é constituída de duas duplas:*/}
                    </Typography>
                    <Box mt={10}>
                        <TeamList peoples={props.peoples}/>
                    </Box>
                </CustomContainer>
                <BgCustom  pb={8}>
                    <CustomContainer >

                        <Box pt={8} >
                            <Recruitment novelsToTraduct={props.novelsToTraduct} novelsToReview={props.novelsToReview}/>
                        </Box>
                    </CustomContainer>
                </BgCustom>
            </Box>
        </Template>
    )
}

const BgCustom = styled(Box)(({theme}) => ({
    backgroundColor: theme.palette.mode === 'light' ? 'white' : FULL_BLACK
}));


