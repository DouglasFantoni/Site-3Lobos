import React from 'react';
import ReactDOM from 'react-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';
import {
    Avatar,
    Button,
    Divider,
    Grid, Link,
    Paper,
    TextField,
    ToggleButton,
    ToggleButtonGroup,
    Typography
} from "@mui/material";
import {Box} from "@mui/system";
import {Social} from "../../../constants/SocialLinks";
import {Novel} from "../../../models/novel";
import {getFile, getFilePath} from "../../../utils/helpers/files_helper";
import Titlebadge from "../../../ui/components/Titlebadge/Titlebadge";
import Title from "../../../ui/components/Title/Title";
import {getTextareaProps} from "../../../utils/helpers/form_helper";

const validationSchema = yup.object({
    nickDiscord: yup
        .string()
        .min(1, 'Preencha este campo')
        .required('Preencha este campo'),
});

interface Props {
    novelsToTraduct: Array<Novel>,
    novelsToReview: Array<Novel>,

}

const Recruitment = (props: Props) => {

    const [novelToReview, setNovelToReview] = React.useState('');
    const [novelToTraduct, setNovelToTraduct] = React.useState('');


    const formik = useFormik({
        initialValues: {
            nickDiscord: '',
        },
        validationSchema: validationSchema,
        onSubmit: (values) => {
            alert(JSON.stringify(values, null, 2));
        },
    });


    const handleSetNovel = (id: string, newNovel: string) => {

        // @ts-ignore
        if (id === 'secReview') {
            setNovelToReview(newNovel);
            setNovelToTraduct('')
        } else {
            setNovelToTraduct(newNovel)
            setNovelToReview('')
        }
    };
    const IMAGE_SIZE = 100

    const SHOW_BOXES = (novelToTraduct !== '' || novelToReview !== '')

    console.log(props.novelsToReview)

    return (
        <div>
            <Typography textAlign={'center'} variant={'h1'} fontWeight={'bold'}>Entre para essa família!</Typography>
            <Typography mt={1} textAlign={'center'} variant={'h2'}>Envie para nós qual obra você tem interesse em traduzir ou revisar que entraremos em contato com você</Typography>
            <form onSubmit={formik.handleSubmit}>
                <Box mt={6}>
                    <Title variant={'h2'}>Selecione uma obra para você revisar</Title>
                </Box>
                <ToggleButtonGroup
                    size="small"
                    exclusive
                    value={novelToReview}
                    color={'info'}

                    onChange={(event, newNovel: string) => handleSetNovel('secReview', newNovel)}
                    sx={{marginX: -2}}

                    aria-label="text formatting"
                >
                        {
                            props.novelsToReview.map((novel) => {

                                console.log(getFile(novel.cover))
                                return (
                                        <ToggleButton   sx={{padding: 2, marginTop: 8, maxWidth: 200, display: 'flex', flexFlow: 'column', width: '100%', marginX: 2, borderLeft: '1px solid rgba(0, 0, 0, 0.12) !important'}}  value={novel.$id} key={novel.$id}  >
                                            <Box pb={1} sx={{ marginTop: -10, display: 'flex', justifyContent: 'center'}}>
                                                <Avatar variant={"square"} alt="Cindy Baker" src={getFilePath(novel.cover)}
                                                        sx={{ width: IMAGE_SIZE, height: 130,}}
                                                />
                                            </Box>

                                            <Box >
                                                <Typography sx={{my: 2}} textAlign={'center'} variant={'h4'}>{novel.title}</Typography>
                                            </Box>

                                        </ToggleButton>
                                    )
                                }
                            )
                        }
                </ToggleButtonGroup>

                <Box mt={6}>
                    <Title variant={'h2'}>Selecione uma obra para você traduzir</Title>
                </Box>
                <ToggleButtonGroup
                    size="small"
                    exclusive
                    value={novelToTraduct}
                    onChange={(event, newNovel: string) => handleSetNovel('', newNovel)}
                    sx={{marginX: -2}}
                    color={'info'}

                    aria-label="text formatting"
                >
                    {
                        props.novelsToTraduct.map((novel) => {

                                console.log(getFile(novel.cover))
                                return (
                                    <ToggleButton  sx={{padding: 2, marginTop: 8, maxWidth: 200, display: 'flex', flexFlow: 'column', width: '100%', marginX: 2, borderLeft: '1px solid rgba(0, 0, 0, 0.12) !important'}}  value={novel.$id} key={novel.$id}  >
                                        <Box pb={1} sx={{ marginTop: -10, display: 'flex', justifyContent: 'center'}}>
                                            <Avatar variant={"square"} alt="Cindy Baker" src={getFilePath(novel.cover)}
                                                    sx={{ width: IMAGE_SIZE, height: 130,}}
                                            />
                                        </Box>

                                        <Box >
                                            <Typography sx={{my: 2}} textAlign={'center'} variant={'h4'}>{novel.title}</Typography>
                                        </Box>

                                    </ToggleButton>
                                )
                            }
                        )
                    }
                </ToggleButtonGroup>
                <Box mt={6}>
                    { SHOW_BOXES ? (
                        <Grid container spacing={2}>
                            <Grid  item  sm={6} xs={12} >
                                <TextField
                                    fullWidth
                                    variant="outlined"
                                    color={'info'}
                                    label="Seu nick do discord"
                                    {...getTextareaProps('nickDiscord', formik)}
                                    onChange={formik.handleChange}
                                />
                            </Grid>

                        </Grid>

                    ) : null}
                </Box>
                <Box justifyContent={'end'} display={'flex'}>
                    <Button sx={{mt: 2,}}  color="info" variant="contained" disabled={((!formik.isValid))}  type="submit">
                        <Typography variant={'h3'} p={1} color={'primary'}>Enviar</Typography>
                    </Button>
                </Box>
            </form>
        </div>
    );
};

export default Recruitment;

