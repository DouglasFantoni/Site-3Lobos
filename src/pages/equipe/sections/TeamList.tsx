import {Avatar, Divider, Grid, Link, Paper, Typography} from "@mui/material";
import {Box} from "@mui/system";
import useSWR from "swr";
import User from "../../../models/user";
import {Social, SocialIcons} from "../../../constants/SocialLinks";

interface Props {
    peoples:  Array<User>,
}

export default function TeamList({peoples}: Props) {


    const IMAGE_SIZE = 100

    return (
        <Grid container spacing={2}>
            {
                peoples.map((people) => (
                        <Grid key={people.$id} item my={2} sm={4} xs={12} >
                            <Paper sx={{padding: 2, marginTop: 8}} >
                                <Box pb={1} sx={{ marginTop: -10, display: 'flex', justifyContent: 'center'}}>
                                    <Avatar variant={"square"} alt="Cindy Baker" src={people.prefs.avatar}
                                            sx={{
                                                width: IMAGE_SIZE,
                                                height: IMAGE_SIZE,
                                                borderRadius: '5px',
                                                border: 'solid 2px #ef7000;'

                                    }}
                                    />
                                </Box>

                                <Box>
                                    <Typography sx={{my: 2}} textAlign={'center'} fontWeight={'bold'} variant={'h2'}>{people.name}</Typography>
                                    <Typography sx={{fontStyle: 'italic'}} variant={'h4'}>{people.prefs.description}</Typography>
                                    <Divider sx={{my: 2}} variant={'middle'}/>
                                    <Typography variant={'h3'}>{people.prefs.occupation}</Typography>
                                </Box>
                                <Box display={'flex'} justifyContent={'center'}>

                                    {people.prefs.social.map((link, index) => {
                                        const socialMedia = new Social(link);
                                        console.log(socialMedia)
                                        return (
                                            <Link color="text.primary" aria-details={socialMedia.name} key={index} href={socialMedia.link} >
                                                {socialMedia.icon}
                                            </Link>
                                        )
                                    })}

                                </Box>
                            </Paper>
                        </Grid>
                    )
                )
            }
        </Grid>

    )
}
