import type {AppProps} from 'next/app'
import React, {useEffect, useState} from 'react'
import createEmotionCache from "../utils/others/createEmotionCache";
import {CacheProvider, EmotionCache} from "@emotion/react";
import {CssBaseline} from "@mui/material";
import CustomThemeProvider from "../utils/providers/CustomThemeProvider";
import AppContent from "../ui/components/AppContent/AppContent";
import Head from 'next/head';
import SessionProvider from "../utils/providers/SessionProvider";
// Client-side cache, shared for the whole session of the user in the browser.
const clientSideEmotionCache = createEmotionCache();
interface MyAppProps extends AppProps {
    emotionCache?: EmotionCache;
}

export default function MyApp(props: MyAppProps) {

    // useEffect(() => {
    //     // Remove the server-side injected CSS.
    //     const jssStyles = document.querySelector('#jss-server-side');
    //     if (jssStyles) {
    //         jssStyles.parentElement!.removeChild(jssStyles);
    //     }
    // }, []);

    useEffect(() => {
        const jssStyles = document.querySelector('#jss-server-side')
        if (jssStyles) {
            // @ts-ignore
            jssStyles.parentElement.removeChild(jssStyles)
        }
    }, [])
    const [isMounted, setIsMounted] = useState(false)
    useEffect(() => {
        setIsMounted(true)
    }, [])


    const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;
    return (
            <CacheProvider value={emotionCache}>
                <Head>
                    {/*<title>My page</title>*/}
                    <meta
                        name="viewport"
                        content="minimum-scale=1, initial-scale=1, width=device-width"
                    />
                </Head>
                <SessionProvider >
                    <CustomThemeProvider>
                        <AppContent >
                            {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
                            <CssBaseline />
                            {isMounted && <Component {...pageProps} />}
                        </AppContent>
                    </CustomThemeProvider>
                </SessionProvider>
            </CacheProvider>
    );
}

// MyApp.propTypes = {
//     Component: PropTypes.elementType.isRequired,
//     emotionCache: PropTypes.object,
//     pageProps: PropTypes.object.isRequired,
// };
