import Image from "next/image";
import localImageLoader from "../utils/others/localImageLoader";
import {Box} from "@mui/system";
import Link from "next/link";
import Button from "@mui/material/Button";
import {ArrowBack, Home} from "@mui/icons-material";
import * as React from "react";
import {Typography} from "@mui/material";
import {HIGHLIGHT} from "../constants/ui/colors";

export default function Custom404() {

    const image = `images/illustrations/404.jpg`;

    const width = 800;
    const height = 591;

    return (
        <>
            <Box mt={4} display={'flex'} sx={{flexFlow: 'column'}} justifyContent={'center'} alignItems={'center'}>
                <Image loader={localImageLoader} src={image} alt="Não encontramos sua página" title={'Página não encontrada'} color="white" width={width} height={height} />
                <Typography mt={6} variant={'h2'}>Oops, não encontramos o que você estava procurando</Typography>
                <Box mt={4}>
                    <Link href={`/`} passHref>
                        <Button sx={{background: HIGHLIGHT, color: 'white', '&:hover': {
                            background: 'blue'
                            }}}   variant="contained"  startIcon={<Home sx={{color: 'white'}}/>}>
                            Traga-me de volta
                        </Button>
                    </Link>
                </Box>
            </Box>
        </>
    )
}
