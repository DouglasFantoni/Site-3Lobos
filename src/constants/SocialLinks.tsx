import FacebookIcon from '@mui/icons-material/Facebook';
import {Instagram, Twitter} from "@mui/icons-material";
import DiscordIcon from "../ui/Icons/DiscordIcon/DiscordIcon";

export class Social {

    private socialMedias = [
        {
            name: 'Instagram',
            icon: <Instagram/>
        },
        {
            name: 'Facebook',
            icon: <FacebookIcon/>
        }, {
            name: 'Twitter',
            icon: <Twitter/>

        }, {
            name: 'Discord',
            icon: <DiscordIcon/>

        }
    ]

    public icon: any;
    public name = '';

    constructor(
        public link: string,
    ) {
        this.socialMedias.map(social => {

            if (link.includes(social.name.toLowerCase())){
                this.icon = social.icon;
                this.name = social.name;
            }
        })
    }
}

export  const SocialIcons: Array<Social> = [
    new Social('https://www.instagram.com/3lobosoficial/'),
    new Social('https://www.facebook.com/3lobosoficial/'),
    new Social('https://twitter.com/3lobosoficial'),
    new Social('https://discord.com/invite/3Scz4ab'),
]
