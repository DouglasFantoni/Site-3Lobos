import {Book, Home, Money, Paid, People} from "@mui/icons-material";
import {OverridableStringUnion} from "@mui/types";
import {SvgIconPropsColorOverrides} from "@mui/material/SvgIcon/SvgIcon";


export class MenuItemType {


    constructor(
        public name: string,
        public icon: any,
        public route: string ) {
    }

}

const menuDefaultProps = {
    sx: {
        fontSize: '26px'
    },
    color: 'secondary' as OverridableStringUnion<
        | 'inherit'
        | 'action'
        | 'disabled'
        | 'primary'
        | 'secondary'
        | 'error'
        | 'info'
        | 'success'
        | 'warning',
        SvgIconPropsColorOverrides
        >,
}

export const menuList = {
    '/home': new MenuItemType('3 Lobos', <Home  {...menuDefaultProps}/>, '/home'),
    '/novels': new MenuItemType('Novels', <Book  {...menuDefaultProps}/>, '/novels'),
    '/equipe': new MenuItemType('Equipe', <People {...menuDefaultProps}/>, '/equipe'),
    '/doacoes': new MenuItemType('Doações', <Paid {...menuDefaultProps}/>, '/doacoes'),

};


// '/doacoes': new MenuItemType('Doações', <AttachMoney {...menuDefaultProps} />, '/novelDetails'),
// '/editoria': new MenuItemType('Editoria', <Edit {...menuDefaultProps} />, '/novelDetails'),





