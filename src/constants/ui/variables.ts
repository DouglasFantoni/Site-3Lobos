

export const CONTAINER_WIDTH = 1200;
export const CONTAINER_SMALL_WIDTH = 900;

export const MOBILE_WIDTH = 343;

export const SPACE_BETWEEN_SECTIONS = 10;
export const SPACE_INSIDE_SECTIONS = 6;
export const FONT_SIZE_DEFAULT = 18;

