import {CONTAINER_WIDTH} from "./variables";


export const MD = CONTAINER_WIDTH;
export const SM = 600;
export const XS = 0;

export declare type Breakpoints = number;
