import {BLACK, MAIN_DARK, MAIN_LIGHT, WHITE} from "./colors";

// import {ThemeState} from "../../utils/providers/CustomThemeProvider";


export interface ThemeConfig {
    name: string,
    bg_primary: string,
    bg_secondary: string,
    text_primary: string,
    text_secondary: string,
}

// export const SERPIA: ThemeConfig = {
//     name: 'Serpia',
//
// }
// bg_dark: MAIN_DARK,
// bg_light: MAIN_LIGHT,
// text_dark: BLACK,
// text_light: MAIN_LIGHT,

export const LIGHT: ThemeConfig = {
    name: 'Light' as TYPE_THEMES,
    bg_primary: WHITE,
    bg_secondary: MAIN_DARK,
    text_primary: BLACK,
    text_secondary: MAIN_LIGHT,
}

export const DARK: ThemeConfig = {
    name: 'Dark' as TYPE_THEMES,
    bg_primary: BLACK,
    bg_secondary: WHITE,
    text_primary: MAIN_LIGHT,
    text_secondary: BLACK,
}

export const SERPIA: ThemeConfig = {
    name: 'Serpia' as TYPE_THEMES,
    bg_primary: MAIN_DARK,
    bg_secondary: MAIN_DARK,
    text_primary: MAIN_DARK,
    text_secondary: MAIN_LIGHT,

}

export const THEMES = {
    [LIGHT.name as TYPE_THEMES]: LIGHT,
    [DARK.name as TYPE_THEMES]: DARK,
    [SERPIA.name as TYPE_THEMES]: SERPIA,
}


export type TYPE_THEMES = 'Light' | 'Dark' | 'Serpia';

export const DEFAULT_THEME  = LIGHT.name;


// export default function getTheme(theme: string): ThemeState {
//     return {
//         name: theme,
//         themeConfig: THEMES[theme]
//     };
// }
