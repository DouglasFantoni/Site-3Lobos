


export const MAIN_LIGHT = '#eeeff9';
export const MAIN_DARK = '#445760';
export const BLACK = '#2b373d';
export const FULL_BLACK = '#1c262a';
export const WHITE = '#fcfcfc';
export const WHITE_LOW = '#d4d4d4';
export const BLACK_LOW = '#d2d2d2';
export const HIGHLIGHT = '#646ed2';
export const SERPIA = '#fffae9';
export const SERPIA_DARK = '#fffae9';

export const COLOR_LINK = '#06128c';
export const DESTACH = COLOR_LINK;
export const DARK_BLUE = '#2c4451';


