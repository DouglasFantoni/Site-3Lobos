import {MD, SM, XS} from './breakpoints';
import {FONT_PRIMARY} from "./fonts";
import {Breakpoint, color, createBreakpoints,} from "@mui/system";
import {PaletteMode, ThemeOptions} from "@mui/material";
import {
    BLACK,
    COLOR_LINK,
    DARK_BLUE,
    FULL_BLACK,
    HIGHLIGHT,
    MAIN_DARK,
    MAIN_LIGHT,
    SERPIA,
    WHITE,
    WHITE_LOW
} from "./colors";
import React from "react";
import {TYPE_THEMES} from "./themes";
import sx from "@mui/system/sx";

const defaultTextOptions = {
    fontFamily: FONT_PRIMARY,
};


declare module "@mui/material/styles" {
    interface BreakpointOverrides {
        xs: true; // removes the `xs` breakpoint
        sm: true;
        md: true;
        lg: false;
        xl: false;
    }
    // allow configuration using `createTheme`
    interface SessionOptions {
        status?: {
            danger?: React.CSSProperties['color'];
        };
    }

    interface Palette {
        blue: Palette['primary'];
    }
    interface PaletteOptions {
        blue: PaletteOptions['primary'];
    }

    // interface PaletteColor {
    //     darker?: string;
    // }
    // interface SimplePaletteColorOptions {
    //     darker?: string;
    // }
}
declare module '@mui/material/Button' {
    interface ButtonPropsVariantOverrides {
        outlinedWhite: true;
    }
}

const BREAKPOINTS = {
    xs: XS,
    sm: SM,
    md: MD,
};


const PALETTE_DARK = {
    primary: {
        light:   MAIN_DARK,
        main: BLACK,
        dark: FULL_BLACK,
    },
    secondary: {
        main: WHITE,
        light: WHITE,
        dark: WHITE_LOW,
    },
    text: {
        primary:  MAIN_LIGHT,
        secondary:  MAIN_LIGHT,
    },
    blue: {
        light: HIGHLIGHT ,
        main: HIGHLIGHT,
        dark: DARK_BLUE,
        neutral: COLOR_LINK
    },
}

const PALETTE_WHITE = {
    primary: {
        light: WHITE,
        main: WHITE,
        dark:  SERPIA,
    },
    secondary: {
        light:   MAIN_DARK,
        main: BLACK,
        dark: FULL_BLACK,
    },
    text: {
        primary:  BLACK,
        secondary:  BLACK,
        // secondary:  MAIN_LIGHT,
    },
    blue: {
        light: HIGHLIGHT ,
        main: COLOR_LINK,
        dark: MAIN_LIGHT,
        neutral: COLOR_LINK
    },
}


export const buildTheme = (currentTheme: TYPE_THEMES): ThemeOptions => {

    const isDark = (currentTheme === 'Dark');

    const selectedTheme = Object.assign((isDark ? PALETTE_DARK : PALETTE_WHITE));

    return {
        palette: {
            mode: currentTheme.toLowerCase() as PaletteMode,
            ...selectedTheme
        },
        typography: {
            h1: {
                fontSize: 34,
                ...defaultTextOptions
            },
            h2: {
                fontSize: 22,
                ...defaultTextOptions

            },
            h3: {
                fontSize: 18,
                ...defaultTextOptions

            },
            h4: {
                fontSize: 14,
                ...defaultTextOptions

            },
            button: {
                fontSize: 16,
                ...defaultTextOptions
            },

            body2: {
                fontSize: 16,
                ...defaultTextOptions
            }

        },
        breakpoints: createBreakpoints({
            // Define custom breakpoint values.
            // These will apply to Material-UI components that use responsive
            // breakpoints, such as `Grid` and `Hidden`. You can also use the
            // theme breakpoint functions `up`, `down`, and `between` to create
            // media queries for these breakpoints
            values: BREAKPOINTS,
            keys: Object.keys(BREAKPOINTS) as Breakpoint[],
        }),
        components: {
            // Name of the component
            MuiButton: {
                styleOverrides: {
                    // Name of the slot
                    root: {
                        // Some CSS
                        color: selectedTheme.secondary.main,
                    },
                    outlined: {
                        border: `1px solid ${MAIN_DARK}`,
                        ":hover": {
                            backgroundColor: '#0b09262b',
                            border: '1px solid gray'
                            ,
                        },
                    },
                },
                variants: [
                    {
                        props: {
                            variant: 'outlinedWhite'
                        },
                        style: {
                            color: MAIN_LIGHT,
                            border: `1px solid ${MAIN_LIGHT}`,
                            ":hover": {
                                backgroundColor: '#0b09262b',
                                border: '1px solid gray',
                            },
                        }
                    }
                ]
            },
            MuiLink: {
                styleOverrides: {
                    // Name of the slot
                    root: {
                        fontSize: 16,
                        ...defaultTextOptions
                    },
                },
            },
            // MuiInputLabel: {
            //     styleOverrides: {
            //         // Name of the slot
            //         root: {
            //             fontSize: 16,
            //             ...defaultTextOptions,
            //             color: selectedTheme.text.primary,
            //
            //
            //         },
            //     },
            // },
            // MuiTextField: {
            //     styleOverrides: {
            //         root: {
            //             InputProps: {
            //                 style: {
            //                     color: selectedTheme.text.secondary
            //                 }
            //             }
            //         }
            //     }
            // },

        }
    };
}


//
// const  = createMuiTheme({
//     palette: {
//         primary: { main: primary },
//         secondary: { main: secondary },
//         common: {
//             black,
//             darkBlack
//         },
//         warning: {
//             light: warningLight,
//             main: warningMain,
//             dark: warningDark
//         },
//         // Used to shift a color's luminance by approximately
//         // two indexes within its tonal palette.
//         // E.g., shift from Red 500 to Red 300 or Red 700.
//         tonalOffset: 0.2,
//         background: {
//             default: background
//         },
//         spacing
//     },
//
//     border: {
//         borderColor: borderColor,
//         borderWidth: borderWidth
//     },
//     overrides: {
//         MuiExpansionPanel: {
//             root: {
//                 position: "static"
//             }
//         },
//         MuiTableCell: {
//             root: {
//                 paddingLeft: spacing * 2,
//                 paddingRight: spacing * 2,
//                 borderBottom: `${borderWidth}px solid ${borderColor}`,
//                 [`@media (max-width:  ${sm}px)`]: {
//                     paddingLeft: spacing,
//                     paddingRight: spacing
//                 }
//             }
//         },
//         MuiDivider: {
//             root: {
//                 backgroundColor: borderColor,
//                 height: borderWidth
//             }
//         },
//         MuiPrivateNotchedOutline: {
//             root: {
//                 borderWidth: borderWidth
//             }
//         },
//         MuiListItem: {
//             divider: {
//                 borderBottom: `${borderWidth}px solid ${borderColor}`
//             }
//         },
//         MuiDialog: {
//             paper: {
//                 width: "100%",
//                 maxWidth: 430,
//                 marginLeft: spacing,
//                 marginRight: spacing
//             }
//         },
//         MuiTooltip: {
//             tooltip: {
//                 backgroundColor: darkBlack
//             }
//         },
//         MuiExpansionPanelDetails: {
//             root: {
//                 [`@media (max-width:  ${sm}px)`]: {
//                     paddingLeft: spacing,
//                     paddingRight: spacing
//                 }
//             }
//         }
//     },
//     typography: {
//         useNextVariants: true
//     }
// });
