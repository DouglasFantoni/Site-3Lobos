

export const COLL_NOVELS = "61720136a083d";
export const COLL_VOLUMES = "6172034de191d";
export const COLL_CHAPTERS = "617203a0df84c";
export const COLL_NEWS = "617bf38725e23";
export const COLL_CONTENTS = "617bf7efe02d0";
export const COLL_GENRES = "617eabba922f0";
export const COLL_TAGS = "617f28558a48a";
export const COLL_NACIONALITY = "617f287541afd";
export const COLL_HISTORY = "617f28b4d53b1";
export const COLL_SCORE = "617f2a7c37775";
export const COLL_PARTNERS = "617f2b2921324";
export const COLL_FAVORITES = "6182c665e0e24";
