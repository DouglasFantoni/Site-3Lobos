import {Badge, IconButton} from "@mui/material";
import NotificationsIcon from "@mui/icons-material/Notifications";
import React from "react";


export default function Notifications() {


    return (
        <>
            <IconButton aria-label="show 17 new notifications" color="inherit">
                <Badge badgeContent={17} color="primary">
                    <NotificationsIcon />
                </Badge>
            </IconButton>
        </>
    )
}
