import {Button, Container, IconButton, Menu, Stack, ToggleButton, ToggleButtonGroup, Typography} from "@mui/material";
import {MouseEvent} from "react";
import {MENU_THEME} from "../Consts_topbar";
import {Add, Remove, Settings} from "@mui/icons-material";
import {usePopupState} from "material-ui-popup-state/hooks";
import {bindMenu, bindTrigger} from "material-ui-popup-state/es";
import {DARK, LIGHT, TYPE_THEMES} from "../../../../../constants/ui/themes";
import {styled} from '@mui/material/styles';
import {useRouter} from "next/router";
import {Box} from "@mui/system";
import {SessionConsumer} from "../../../../../utils/providers/SessionProvider";


export default function ThemeConfig ()  {

    const popupState = usePopupState({ variant: 'popover', popupId: MENU_THEME});
    const router = useRouter();
    const showFontsize = router.pathname.includes('/capitulos');
    // console.log('showFontsize', showFontsize)

    return (
        <>
            <Button
                id={MENU_THEME}
                aria-label="Configurações de acessibilidade"
                sx={{mr: 2, paddingBottom: 0}}


                {...bindTrigger(popupState)}
                startIcon={<Settings color={'secondary'}/>}
            >
                <Text color="textPrimary" variant="h2">Temas</Text>

            </Button>
            <Menu
                disableAutoFocusItem={true}
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
                {...bindMenu(popupState)}
            >

                <SessionConsumer>
                    {({session, setSession}) => {

                        const handleUpdateTheme = (event: MouseEvent<HTMLElement>,value: TYPE_THEMES, ) => {
                            if (value && value !== session.theme){
                                setSession({
                                    ...session,
                                    theme: value
                                });
                            }
                        };

                        const handleUpdateFontsize = (newFontSize: number,) => {
                            if (newFontSize >= 12 && newFontSize <= 42 && newFontSize !== session.fontsize) {

                                setSession({
                                    ...session,
                                    fontsize: newFontSize
                                }) ;
                            }
                        }

                        return (
                            <Container sx={{pt: 2, pb: 2}}>
                                {/*<Typography align="center" color={'secondary'} sx={{mb:1, mt:2}} variant="h3">Temas</Typography>*/}

                                <ToggleButtonGroup
                                    value={session.theme}
                                    exclusive
                                    onChange={handleUpdateTheme}
                                    color="secondary"

                                >
                                    <ToggleButton color="secondary" value={LIGHT.name}>{LIGHT.name}</ToggleButton>
                                    <ToggleButton color="secondary" value={DARK.name}>{DARK.name}</ToggleButton>
                                    {/*{*/}
                                    {/*       page === 'novels' &&*/}
                                    {/*       <ToggleButton color="primary" value={SERPIA.name}>{SERPIA.name}</ToggleButton>*/}
                                    {/*}*/}
                                </ToggleButtonGroup>
                                {   showFontsize ?
                                        <Box>
                                            <Typography align="center" color={'secondary'} sx={{mb: 1, mt: 2}} variant="h3">Tamanho
                                                da fonte</Typography>
                                            <Stack direction="row" sx={{justifyContent: 'center', alignItems: 'center'}}
                                                   spacing={2}>
                                                <IconButton sx={{backgroundColor: 'white'}}
                                                            onClick={(el) => handleUpdateFontsize(session.fontsize - 1)}
                                                            aria-label="remove" color="error">
                                                    <Remove/>
                                                </IconButton>
                                                <Typography align="center" id={'fontsize'} fontWeight={'bold'}
                                                            color={'secondary'} sx={{mb: 1, mt: 2}}
                                                            variant="h3">{session.fontsize}</Typography>
                                                <IconButton sx={{backgroundColor: 'white'}}
                                                            onClick={(el) => handleUpdateFontsize(session.fontsize + 1)}
                                                            aria-label="add" color="success">
                                                    <Add/>
                                                </IconButton>
                                            </Stack>
                                        </Box> : null
                                    }

                            </Container>
                        )
                    }


                    }
                </SessionConsumer>
            </Menu>
        </>

    )
}
const Text = styled(Typography)`
        text-transform: none;
        font-weight: bold;
    `;
