import {Button, Menu, MenuItem, Typography} from "@mui/material";
import React from "react";
import {MENU_USER} from "../Consts_topbar";
import {AccountCircle} from "@mui/icons-material";
import {bindMenu, bindTrigger} from "material-ui-popup-state/es";
import {usePopupState} from "material-ui-popup-state/hooks";
// import { styled } from '@mui/material/styles';
import {styled} from '@mui/material/styles';

const Div = styled('div')``;


export default function User() {

    const popupState = usePopupState({ variant: 'popover', popupId: MENU_USER})


    return (
        <>
            <Button
                aria-controls={MENU_USER}
                {...bindTrigger(popupState)}
                startIcon={<AccountCircle/>}
            >

                <Text color="textPrimary" variant="h2">Entrar</Text>
            </Button>
            <Menu
                MenuListProps={{
                    'aria-labelledby': 'basic-button',
                }}
                {...bindMenu(popupState)}
            >
                <MenuItem onClick={popupState.close}>Profile</MenuItem>
                <MenuItem onClick={popupState.close}>My account</MenuItem>
                <MenuItem onClick={popupState.close}>Logout</MenuItem>
            </Menu>
        </>
    )
}
const Text = styled(Typography)`
        text-transform: none;
        font-weight: bold;
    `;
