import {AppBar, Toolbar,} from "@mui/material";
import React from "react";
import Logorounded from "../../../components/Logo-rounded/Logo-rounded";
import {BLACK_LOW} from "../../../../constants/ui/colors";
import {DesktopMenu} from "./DesktopMenu/DesktopMenu";
import MobileMenu from "./MobileMenu/MobileMenu";
import {Box} from "@mui/system";
import CustomContainer from "../../../components/Container/CustomContainer";
import useWindowSize from "../../../../utils/helpers/useWindowSize";
import {MD} from "../../../../constants/ui/breakpoints";
import Socialicons from "../../Socialicons/Socialicons";

export default function Topbar (){


    const {  width, height } = useWindowSize();

    let menu, socialIcons;

    if (width > MD){
        socialIcons =  <Socialicons/>;

        menu = <DesktopMenu  />;
    }else{
        menu = <MobileMenu handleMobileMenuClose={() => {}} handleMobileMenuOpen={() => {}}/>
    }

    return (

        <Box sx={{ flexGrow: 1, position: 'relative', zIndex: 10}}>
            <AppBar position="static" enableColorOnDark={true} sx={{boxShadow: 'none', borderBottom: `1px solid ${BLACK_LOW}`}}>
                <CustomContainer>
                    {socialIcons}

                    <Toolbar disableGutters >

                        <Logorounded/>
                        {menu}

                    </Toolbar>

                </CustomContainer>
            </AppBar>
        </Box>
    );
}
