

const MENU_THEME = 'menu_theme';
const MENU_NOTIFICATIONS = 'menu_notifications';
const MENU_USER = 'menu_user';

interface MenuStateProps {
    menu_theme: boolean,
    menu_notifications: boolean,
}

export class MenuState implements MenuStateProps{

    menu_theme = false;
    menu_notifications =false;
}

export {MENU_NOTIFICATIONS, MENU_THEME, MENU_USER}
