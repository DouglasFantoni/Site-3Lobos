import React from "react";
import MenuList from "../MenuList/MenuList";
import ThemeConfig from "../ThemeConfig/ThemeConfig";
import {Box, styled} from "@mui/system";

interface Props {
}


export const DesktopMenu = ({}: Props) => {

    return (
        <>
            <Wrapper>
                <MenuList alignment="horizontal"/>
            </Wrapper>
            <Box sx={{ flexGrow: 1 }} />

            <menu style={{padding: 0}}>
                <ThemeConfig/>
                {/*<User/>*/}
            </menu>
        </>
    )
}

const Wrapper = styled('div')(({theme}) => ({
        display: 'flex',
}))

