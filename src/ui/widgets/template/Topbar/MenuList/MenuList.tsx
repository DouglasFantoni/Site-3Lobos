import {Box, styled} from "@mui/system";
import {List, ListItemButton, ListItemIcon, Typography} from "@mui/material";
import {DESTACH, WHITE} from "../../../../../constants/ui/colors";
import React from "react";
import {useRouter} from "next/router";
import {menuList} from "../../../../../constants/menus";


const VERTICAL = 'vertical';
const HORIZONTAL = 'horizontal';

interface Props {
    alignment: 'vertical' | 'horizontal',
}

export default function MenuList ({alignment}: Props){

    const router = useRouter();
    const actualPage = router.pathname;

    const configs = {
        displayList: alignment === VERTICAL ? 'block' : 'flex',
        minWidth: alignment === VERTICAL ? '53px' : '34px'
    }

    return (

        <Box component="menu" sx={{p: 0, ml: alignment == VERTICAL ?  'none' : 14, mb: 0}}>
            <List sx={{display: configs.displayList}}>
                {Object.keys(menuList).map((menu: string , index) => {
                    // @ts-ignore
                    const menuItem = menuList[menu] as MenuItemType;
                    const isSelected = actualPage.includes(menuItem.route);

                    return (
                        <CustomButtom onClick={(event) => {
                            router.push(menuItem.route)}} selected={isSelected}  key={menuItem.name}>
                            <WrapText>
                                <ListItemIcon sx={{minWidth: configs.minWidth}}>
                                    {menuItem.icon}
                                </ListItemIcon>
                                <Text fontWeight={isSelected ? 'bold' : 'normal'} color="text.primary" variant="h2">{menuItem.name}</Text>
                            </WrapText>
                        </CustomButtom>
                    )
                })}
            </List>
        </Box>
    )
}

const Text = styled(Typography)`
    `;

const CustomButtom = styled(ListItemButton)(({theme}) => ({
    '&.Mui-selected  > div': {
        borderBottom: `2px solid ${theme.palette.mode === 'dark' ? WHITE : DESTACH}`,
    }

}))


const WrapText = styled(Box)`
    display: flex;
    padding-bottom: 5px;

    `;
