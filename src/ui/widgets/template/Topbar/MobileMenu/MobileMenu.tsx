import {Divider, Drawer, IconButton, List, ListItem} from "@mui/material";
import React, {useState} from "react";
import {Box, styled} from "@mui/system";
import MenuIcon from "@mui/icons-material/Menu";
import ThemeConfig from "../ThemeConfig/ThemeConfig";
import User from "../User/User";
import Socialicons from "../../../Socialicons/Socialicons";
import MenuList from "../MenuList/MenuList";


const MobileMenu = ({handleMobileMenuClose, handleMobileMenuOpen}: {handleMobileMenuClose: any, handleMobileMenuOpen: any}) => {

    const [state, setState] = useState(false);

    const toggleDrawer =
        () =>
            (event: React.KeyboardEvent | React.MouseEvent) => {
                if (
                    event.type === 'keydown' &&
                    ((event as React.KeyboardEvent).key === 'Tab' ||
                        (event as React.KeyboardEvent).key === 'Shift')
                ) {
                    return;
                }

                setState(!state);
            };


    return (
            <>
                <Wrapper >
                    <Box sx={{ flexGrow: 1}}/>
                    <ThemeConfig/>

                    <IconButton
                        edge="start"

                        color="inherit"
                        aria-label="open drawer"
                        aria-controls="basic-menu"
                        onClick={toggleDrawer()}
                        id="basic-button"

                    >
                        <MenuIcon />
                    </IconButton>

                </Wrapper>
                <Drawer
                    anchor="right"
                    open={state}
                    onClose={toggleDrawer()}
                >

                    <Box
                        role="presentation"
                        onClick={toggleDrawer()}
                        onKeyDown={toggleDrawer()}
                        sx={{p: 2}}

                    >

                        {/*<List>*/}
                        {/*    <ListItem button sx={{justifyContent: 'center'}}>*/}
                        {/*        <User/>*/}
                        {/*    </ListItem>*/}
                        {/*</List>*/}

                        {/*<Divider />*/}

                        <MenuList alignment="vertical"/>
                        <List>
                            <ListItem >
                                <Socialicons/>
                            </ListItem>
                        </List>
                    </Box>


                </Drawer>
            </>
    )
}

const Wrapper = styled('div')(({theme}) => ({
    [theme.breakpoints.down('md')]: {
        display: 'flex  '
    },
    flexGrow: 1,
    display: 'none'
}))

export  default MobileMenu;
