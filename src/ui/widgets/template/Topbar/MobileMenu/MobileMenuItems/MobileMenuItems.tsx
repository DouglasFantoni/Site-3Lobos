import React from "react";
import {List, ListItem, ListItemIcon, ListItemText} from "@mui/material";
import {menuList} from "../../../../../../constants/menus";

export default function MobileMenuItems() {

    return (
        <>
            <List>
                {Object.values(menuList).map((menu, index) => (
                    <ListItem onClick={(event) => {}} selected={index === 1} button key={menu.name}>
                        <ListItemIcon>
                            {menu.icon}
                        </ListItemIcon>
                        <ListItemText primary={menu.name} />
                    </ListItem>
                ))}
            </List>

        </>

    )
}
