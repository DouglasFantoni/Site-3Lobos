import {Box, Container, Divider, Typography} from "@mui/material";
import {DARK_BLUE, WHITE} from "../../../../constants/ui/colors";

const Footer = () => {

    return (
        <Box sx={{backgroundColor: DARK_BLUE}} >
            <Container maxWidth="md">
                {/*<Line/>*/}
                {/*<Divider sx={{py: 5}}/>*/}
                <Box sx={{pt: 10, pb:2, mx: "auto", width: "max-content"}}>
                    <Typography color={WHITE} variant="h3">Copyright 3Lobos 2021</Typography>
                </Box>
            </Container>
        </Box>
    )
}


export default Footer;
