import React from "react";
import Head from "next/head";
import Topbar from "./Topbar/Topbar";
import Footer from "./Footer/Footer";
import AppContent from "../../components/AppContent/AppContent";


export default function Template ({children, title}: {children: any, title: string}) {

    return (
        <>
            <Head>
                <meta
                    name="viewport"
                    content="minimum-scale=1, initial-scale=1, width=device-width"
                />
                <meta name="og:title" property="og:title" content={title}/>

                <title>{title}</title>
            </Head>
            <AppContent >
                <Topbar/>
                {/*<RouterBreadcrumbs/>*/}
                <main style={{boxSizing: 'border-box'}}>{children}</main>
                <Footer/>
            </AppContent>
        </>
    )
}
