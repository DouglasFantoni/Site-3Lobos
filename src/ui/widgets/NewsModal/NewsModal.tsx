import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Typography from '@mui/material/Typography';
import {NewsFull} from "../../../models/news";
import {timestampView} from "../../../utils/helpers/dataHelper";
import {Divider} from "@mui/material";


interface Props {
    isOpen: boolean,
    news: NewsFull,
    handleClose: (event: {}, reason: "backdropClick" | "escapeKeyDown") => void
}

export default function NewsModal({isOpen, handleClose, news}: Props) {

    return (
        <Modal
            sx={{overflow: 'scroll'}}
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={isOpen}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
                sx: {
                    cursor: 'pointer'
                }
            }}
        >

            <Fade in={isOpen}>
                <Box sx={style}>
                    <Typography color={'text.primary'} id="transition-modal-title" variant="h1" >
                        {news.title}
                    </Typography>
                    <Typography sx={{my: 1}} color={'text.primary'} id="transition-modal-title" variant="h4" >
                        Por <b>{news.author}</b> em <b>{timestampView(news.moment)}</b>
                    </Typography>
                    <Divider/>
                    <Typography variant="h2" color={'text.primary'} id="transition-modal-description" sx={{ mt: 2 }}>
                        <div dangerouslySetInnerHTML={{ __html: news.content}} />
                    </Typography>
                </Box>
            </Fade>
        </Modal>
    )
}

const style = {
    position: 'absolute' as 'absolute',
    top: '20%',
    pb: 3,
    left: '50%',
    transform: 'translate(-50%, -100px)',
    width: '80%',
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 4,
};



