import {Box, Card, CardActionArea, CardContent, Typography} from "@mui/material";
import {AccessTime} from "@mui/icons-material";
import NewsModal from "../NewsModal/NewsModal";
import {useState} from "react";
import {NewsFull} from "../../../models/news";
import {timestampView} from "../../../utils/helpers/dataHelper";
import {styled} from "@mui/system";

interface Props {
    news: NewsFull,
}

export default function NoticeCard({news,}: Props) {
    const [open, setOpen] = useState(false);
    const switchStatus = () => {
        setOpen(!open);
    };

    const strCleaned = (news.content as string).replace(/(\r\n|\n|\r)/gm, "");

    return (
        <>
            <Card  sx={{mb: 2,}}>
                <CardActionArea onClick={switchStatus} >
                    <CardContent>
                        <Typography variant="h2" color="text.primary" >
                            {news.title}
                        </Typography>
                        {/*<Typography sx={{    overflow: "hidden",*/}
                        {/*    textOverflow: 'ellipsis', height: '50px'}} variant="h3" color="text.primary" gutterBottom>*/}
                        {/*    <CustomDiv dangerouslySetInnerHTML={{ __html: strCleaned}} />*/}
                        {/*</Typography>*/}
                        <Box sx={{mt: 2, display: 'flex', justifyContent: 'space-between'}}>
                            <Typography variant="h4" color="text.primary" >
                                Por: {news.author}
                            </Typography>
                            <Typography variant="h4"  color="text.primary" display={"flex"}>
                                <AccessTime sx={{typography: 'h4', mr:'3px', mt:'1px'}} />
                                {timestampView(news.moment)}
                            </Typography>
                        </Box>
                    </CardContent>
                </CardActionArea>
            </Card>
            <NewsModal isOpen={open} news={news} handleClose={switchStatus}/>
        </>
    )
}


const CustomDiv = styled('div')(({theme}) => ({

}));
