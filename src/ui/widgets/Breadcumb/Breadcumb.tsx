import * as React from 'react';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import {useRouter} from "next/router";
import {Stack} from "@mui/material";
import NextLink from "next/link";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import {ColorType} from "../../../constants/ui/types";
//
// interface ListItemLinkProps extends ListItemProps {
//     to: string;
//     open?: boolean;
// }
//
// const breadcrumbNameMap = menuList;
//
//
// function ListItemLink(props: ListItemLinkProps) {
//     const { to, open, ...other } = props;
//     const primary = breadcrumbNameMap[0];
//
//     let icon = null;
//     if (open != null) {
//         icon = open ? <ExpandLess /> : <ExpandMore />;
//     }
//
//     return (
//         <li>
//             <ListItem button component={RouterLink as any} to={to} {...other}>
//                 <ListItemText primary={primary} />
//                 {icon}
//             </ListItem>
//         </li>
//     );
// }
//
// interface LinkRouterProps extends LinkProps {
//     to: string;
//     replace?: boolean;
// }
//

interface ListItemLinkProps {
    name: string,
    link: string,
    active: boolean
}

const Breadcrumb = (props: ListItemLinkProps) => {
    const {name, link, active, ...other} = props


    const el = active ? <NextLink href={link} passHref>
                <Link underline="hover"   color={'blue.main'}>
                    {name}
                </Link>
            </NextLink>
        :
            <Typography  color={'text.primary'}>
                {name}
            </Typography>;

    return el
}


interface Props {
    links: {[key: string]: string},
    version?: ColorType
}
export default function CustomBreadcumb({links, version = 'dark'}: Props) {

    const router = useRouter();

    return (

        <Stack spacing={2}>
            <Breadcrumbs separator={<NavigateNextIcon sx={{color: 'blue.light'}} fontSize="small" />} aria-label="breadcrumb">

                {Object.keys(links).map((el, key) =>
                            <Breadcrumb key={key}  link={links[el] } name={el} active={!(Object.keys(links).length-1 === key)}/>
                    )}
            </Breadcrumbs>

        </Stack>
    );



    {/*<LinkRouter underline="hover" color="inherit" to="/">*/}
    {/*    Home*/}
    {/*</LinkRouter>*/}
    {/*{pathnames.map((value: string, index: number) => {*/}
    {/*    const last = index === pathnames.length - 1;*/}
    {/*    const to = `/${pathnames.slice(0, index + 1).join('/')}`;*/}

    {/*    console.log('to', to)*/}

    {/*    return last ? (*/}
    {/*        <Typography color="text.primary" key={to}>*/}
    {/*            {breadcrumbNameMap[to]}*/}
    {/*        </Typography>*/}
    {/*    ) : (*/}
    {/*        <LinkRouter underline="hover" color="inherit" to={to} key={to}>*/}
    {/*            {breadcrumbNameMap[to]}*/}
    {/*        </LinkRouter>*/}
    {/*    );*/}
    {/*})}*/}


        // <MemoryRouter initialEntries={['/inbox']} initialIndex={0}>
        //     <Box sx={{ display: 'flex', flexDirection: 'column', width: 360 }}>
        //         <Route>
        //             {({location}: {location: any}) => {
        //                 const pathnames = location.pathname.split('/').filter((x: any) => x);
        //
        //                 return (

        //                 );
        //             }}
        //         </Route>
        //     </Box>
        // </MemoryRouter>
}
