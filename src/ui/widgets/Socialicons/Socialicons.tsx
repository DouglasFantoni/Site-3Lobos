import {Link, Stack} from "@mui/material";
import {SocialIcons} from "../../../constants/SocialLinks";

export default function Socialicons() {

    return (
        <Stack
            direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={2}
            sx={{ pt:2,}}
        >

            {SocialIcons.map((item, index) => (
                <Link color="text.primary" key={index} href={item.link} >

                    {item.icon}
                </Link>
            ) )}

        </Stack>
    )
}
