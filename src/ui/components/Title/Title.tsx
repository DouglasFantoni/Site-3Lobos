import {
  Box,
  Typography,
  TypographyPropsVariantOverrides,
} from "@mui/material";
import { OverridableStringUnion } from "@mui/types";
import { Variant } from "../../../../node_modules/@mui/material/styles/createTypography";
import { ColorType } from "../../../constants/ui/types";
import Titlebadge from "../Titlebadge/Titlebadge";

interface Props {
  sx?: any;
  type?: ColorType;
  badge?: boolean;
  variant?: OverridableStringUnion<
    Variant | "inherit",
    TypographyPropsVariantOverrides
  >;
  children: any;
}

export default function Title({
  type = "dark",
  sx,
  badge = true,
  variant = "h1",
  children,
}: Props) {
  const Badge = badge ? <Titlebadge type={type} /> : null;

  const color = type === "dark" ? "secondary" : "primary";

  return (
    <Box sx={{ display: "inline-block", mb: 3 }}>
      {Badge}

      <Typography
        variant={variant}
        color={color}
        sx={{ display: "inline-block", ...sx }}
      >
        {children}
      </Typography>
    </Box>
  );
}
