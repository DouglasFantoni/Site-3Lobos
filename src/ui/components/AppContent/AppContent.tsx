import {Box, styled} from "@mui/system";

const Wrapper = styled(Box)((theme) => {
    return ({
            backgroundColor: theme.theme.palette.primary.main
        })
})

export default function AppContent(props: any) {


    return (
        <Wrapper sx={{minHeight: '100vh'}}>
            {props.children}
        </Wrapper>
    )
}
