import {Container} from "@mui/material";

export default function CustomContainer({children,sx }: {children: any, sx?: Object}) {

    return (
        <Container  maxWidth="md" sx={{position: 'relative', ...sx}}>
            {children}
        </Container>
    )
}
