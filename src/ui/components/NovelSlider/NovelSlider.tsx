import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import {Box, styled} from "@mui/system";
import NovelCardVertical from "../NovelCard/NovelCardVertical";
import {MD, SM} from "../../../constants/ui/breakpoints";
import {ChevronLeft, ChevronRight} from "@mui/icons-material";
import CustomContainer from "../Container/CustomContainer";
import useWindowSize from "../../../utils/helpers/useWindowSize";
import {Novel} from "../../../models/novel";


interface Props {
    size?: 'md' | 'sm',
    sx?: any,
    novels: Array<Novel>
}

export default function NovelSlider({size = 'sm', sx, novels}: Props) {

    const {isBreakpoint} = useWindowSize();

    if (!isBreakpoint(MD))
        size = 'sm';

    settings.slidesToShow = settings.slidesToScroll = size === 'md' ? 3 : 4;
    novels.length <= 3 && (settings.slidesToShow = settings.slidesToScroll = 3);


    return (
        <CustomContainer sx={{}}>
            <Box sx={{cursor:'grab', px: '15px',...sx}}>

                <Slider {...settings}>
                    {
                        novels.map((novel, index) => (
                            <CardWrapper key={index}>
                                <NovelCardVertical size={size} novel={novel}/>
                            </CardWrapper>
                        ))
                    }

                </Slider>
            </Box>
        </CustomContainer>

    )
}


const CardWrapper = styled('div')((theme) => ({

}))

const styles = {
    position: "absolute",
    top: "50%",
    padding: 0,
    transform: "translate(0, -50%)",
    cursor: 'pointer',
    color: '#07053ce6',
    right: "-35px"
}

function SimplePrevArrow(props: any) {
    const { className, style, onClick } = props;


    return (
        <div
            // @ts-ignore

            style={{ ...styles, display: "block",   }}
            onClick={onClick}
        >
            <ChevronRight color={'secondary'} sx={{fontSize: 65}} fontSize="large"/>
        </div>
    );
}

function SimpleNextArrow(props: any) {
    const { className, style, onClick } = props;

    return (
        <div
            // @ts-ignore
            style={{ ...styles, left: '-35px', right: 'unset'  }}
            onClick={onClick}
        >
            <ChevronLeft color={'secondary'} sx={{fontSize: 65}}/>
        </div>
    );
}

let settings = {
    infinite: true,
    draggable: true,
    touchMove: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    prevArrow:  <SimpleNextArrow/>,
    nextArrow: <SimplePrevArrow/>,
    initialSlide: 0,
    responsive: [
    {
        breakpoint: MD,
        settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
        }
    },
    {
        breakpoint: SM,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
        }
    },
]
};
