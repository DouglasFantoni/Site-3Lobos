import {Box} from "@mui/system";

export default function BoxCentered({children}: {children: any}) {

    return (
        <Box display={'flex'} justifyContent={'center'}>
            {children}
        </Box>

    )
}
