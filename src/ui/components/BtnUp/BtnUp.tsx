import {useState} from "react";
import {Fab, Zoom} from "@mui/material";
import {useTheme} from "@mui/system";
import {DESTACH} from "../../../constants/ui/colors";
import {ArrowUpward} from "@mui/icons-material";
import useWindowSize from "../../../utils/helpers/useWindowSize";


export default function BtnUp() {
    const [showButton, setShowButton] = useState(false);

    const {width, height} = useWindowSize();

    if (typeof window !== 'undefined') {
        window.addEventListener('scroll', () => {
            window.scrollY > 100 ? setShowButton(true) : setShowButton(false);
        })
    }


    const theme = useTheme();
    const transitionDuration = {
        // @ts-ignore

        enter: theme.transitions.duration.enteringScreen,
        // @ts-ignore

        exit: theme.transitions.duration.leavingScreen,
    };

    return (
        <Zoom
            in={showButton }
            timeout={transitionDuration}
            style={{
                transitionDelay: `${showButton ? transitionDuration.exit : 0}ms`,
            }}
        >
            <Fab onClick={scroolToTop} sx={fabStyle} aria-label={'Add'} color={'primary'}>
                <ArrowUpward/>
            </Fab>
        </Zoom>
    )
}
const scroolToTop = () => {
    window.scrollTo({top: 0,behavior: 'smooth' })
}
const fabStyle = {
    position: 'fixed',
    bottom: 16,
    right: 16,
    color: 'common.white',
    bgcolor: DESTACH,
    ':hover': {
        bgcolor: DESTACH,
    },
};
