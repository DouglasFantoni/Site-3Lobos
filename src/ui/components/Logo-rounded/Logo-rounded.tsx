import Logo from "../Logo/Logo";
import {logo3lobos} from "../.././../constants/images";
import {BLACK_LOW} from "../../../constants/ui/colors";
import {styled} from '@mui/material/styles';

const Wrap = styled('div')((theme) => (`
        background-color: #fff;
        border-radius: 10px 10px 50% 50%;
        // border: 2px solid gray;

        padding: 15px 5px;
        position: absolute;
        box-shadow: 0 0 3px 2px ${BLACK_LOW};
        top: 10px;
    `))

export default function Logorounded({logoName = logo3lobos, }) {

    return (
        <Wrap >
            <Logo logoName={logo3lobos} width="90" height="50"/>
        </Wrap>

    )
}
