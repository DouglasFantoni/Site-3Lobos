import {CircularProgress} from "@mui/material";

// interface Props  {
//     data: null | any,
// }

export default function Spinner ({children, data}: {children: any, data: any}) {
    let Spin = <CircularProgress color="secondary" />;

    if (data)
        Spin = children

    return (
        <>
            {Spin}
        </>
    )
}
