// import { styled } from '@mui/material/styles';
import {DESTACH, HIGHLIGHT} from "../../../constants/ui/colors";
import {styled} from '@mui/material/styles';
import {ColorType} from "../../../constants/ui/types";

const Item = styled('div')(({ type }: {type: any}) => ({
    height: '35px',
    position: 'relative',
    top: '-8px',
    width: '6px',
    marginRight: '8px',
    backgroundColor: type === 'dark' ? DESTACH : HIGHLIGHT,
    display: 'inline-block',
    verticalAlign: 'middle',
}));

interface Props {
    type: ColorType
}

export default function Titlebadge ({type}: Props) {

    return (
        <>
            <Item type={type}/>
        </>
    )
}
