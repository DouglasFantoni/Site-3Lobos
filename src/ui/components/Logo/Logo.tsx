import Image from 'next/image';
import localImageLoader from "../../../utils/others/localImageLoader";

interface Logo_props {
    logoName: string;
    width: string;
    height: string;
}

const Logo = ({logoName, width, height}: Logo_props )=> {
    const logo = `images/logos/${logoName}`;
    return (
        <>
            <Image loader={localImageLoader} src={logo} alt="Logo 3Lobos " title={'3Lobos'} color="white" width={width} height={height} />
        </>
    )
}

export default Logo;
