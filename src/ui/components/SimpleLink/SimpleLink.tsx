import NextLink from "next/link";
import {Link as MUILink} from "@mui/material";
import {COLOR_LINK} from "../../../constants/ui/colors";

interface Props {
    children?: any,
    link: string,
}

export default function SimpleLink({link, children}: Props) {
    const linkComponent = <NextLink href={link} passHref/>;

    return (
        <NextLink href={link} passHref>
            <MUILink  color={COLOR_LINK}>
                {children}
            </MUILink>
        </NextLink>
    )
}
