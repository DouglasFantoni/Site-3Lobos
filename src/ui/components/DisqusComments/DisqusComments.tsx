import {DiscussionEmbed} from "disqus-react";
import {GlobalStyles} from "@mui/material";
import {FONT_PRIMARY} from "../../../constants/ui/fonts";
import {DISCORD_NAME, DISCORD_URL} from "../../../constants/configs";
import {SessionConsumer} from "../../../utils/providers/SessionProvider";

interface Props {
    id: string,
    title: string,
}

export default function DisqusComments({id, title}: Props) {

    return (
        <SessionConsumer>
            {(theme) => {
                // console.log('renderizou')

                return <div>
                    <GlobalStyles styles={{'#disc-component  *': {
                            fontFamily: FONT_PRIMARY,
                            color: 'white'
                        }}} />
                    <div id="disc-component">

                        <DiscussionEmbed
                            shortname={DISCORD_NAME}
                            config={
                                {
                                    url: DISCORD_URL,
                                    identifier: id, //article-id
                                    title: title,
                                    language: 'pt_BR' //e.g. for Traditional Chinese (Taiwan)
                                }
                            }
                        />
                    </div>
                </div>
            }

            }
        </SessionConsumer>
    )
}
//

