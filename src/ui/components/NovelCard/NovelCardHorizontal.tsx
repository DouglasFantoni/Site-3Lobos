import Image from 'next/image'
import {Box, styled} from "@mui/system";
import {Card, CardActionArea, CardContent, Typography} from "@mui/material";
import {AccessTime} from "@mui/icons-material";
import {ChapterFull} from "../../../models/chapter";
import {getFile} from "../../../utils/helpers/files_helper";
import Link from 'next/link'
import LinkCleaned from "../LinkCleaned/LinkCleaned";

interface Props {
    chapter: ChapterFull,
}

// const IMAGE_WIDTH = 140;
// const IMAGE_HEIGHT = 180;

const IMAGE_WIDTH = 90;
const IMAGE_HEIGHT = 120;


export default function NovelCardHorizontal({chapter}: Props) {

    return (
            <CardHorizontal  sx={{ m: 2, width: "auto"}}>
                <CardActionArea >
                    <Link href={`novels/${chapter.slugNovel}/capitulos/${chapter.slug}`} passHref>
                        <LinkCleaned>
                            <CardContent sx={{display: 'flex', p: '0 !important', position: 'relative'}}>
                                <Image
                                    src={getFile(chapter.coverNovel)}
                                    alt={"Capa "+ chapter.titleNovel}
                                    title={chapter.titleNovel}
                                    width={IMAGE_WIDTH}
                                    height={IMAGE_HEIGHT}
                                    // blurDataURL="data:..." automatically provided
                                    // placeholder="blur" // Optional blur-up while loading
                                />
                                <Box sx={{p:2}}>
                                    <Typography variant="h3" fontWeight={"bold"} color="text.primary" gutterBottom>
                                        {chapter.titleNovel}
                                    </Typography>
                                    <Typography variant="h3" color="text.primary" gutterBottom>
                                        {chapter.title}
                                    </Typography>
                                    <Box sx={{display: 'flex', justifyContent: 'space-between', position: 'absolute', bottom: 0, right: 0, p:2}}>
                                        <Box>

                                        </Box>

                                        <Typography variant="h4"  color="text.primary" display={"flex"}>
                                            <AccessTime sx={{typography: 'h4', mr:'3px', mt:'1px'}} />
                                            {chapter.momentFormated}
                                        </Typography>
                                    </Box>
                                </Box>

                            </CardContent>
                        </LinkCleaned>

                    </Link>

                </CardActionArea>

            </CardHorizontal>
    )
}

const CardHorizontal= styled(Card)(({theme}) => ({
    mb: 2,
    width: 'max-content',
    p: 0
}))

const ContainerText = styled('div')((theme) => ({
    bottom: -10,
    left: '10px',
    padding: 15,
    background: '#000',

    '& > *': {
        color: '#fff'
    }
}))
