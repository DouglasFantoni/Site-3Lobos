import Image from 'next/image'
import {Box, styled} from "@mui/system";
import {Rating, Typography} from "@mui/material";
import Link from 'next/link';
import {MOBILE_WIDTH} from "../../../constants/ui/variables";
import {STARS_PRECISION} from "../../../constants/configs";
import {getFile} from "../../../utils/helpers/files_helper";
import {Novel} from "../../../models/novel";

const REDUCE_SIZE = 10;
const sizes: Object = {
    'md': {
        // cardWidth: 250,
        // cardHeight: 350,
        cardWidth: MOBILE_WIDTH - REDUCE_SIZE,
        cardHeight: 450 - REDUCE_SIZE,
    },
    'sm': {
        // cardWidth: 300,
        // cardHeight: 400,
        cardWidth: 250 - REDUCE_SIZE,
        cardHeight: 350 - REDUCE_SIZE,
    },
}

interface Props {
    size: 'md' | 'sm',
    novel: Novel,
}


export default function NovelCardVertical({size, novel}: Props) {

    // @ts-ignore
    const sizeConfig = sizes[size];

    const link = `/novels/${novel.slug}`;

    return (
        <Box sx={{position: 'relative', marginX: 2, marginY: 2}}>
            <Box sx={{position: 'relative',borderRadius: '5px', width: 'max-content', mx: 'auto'}}>
                <Box >
                    <Link href={link} passHref>
                        <a style={{display: 'flex'}}>
                            <Box sx={{boxShadow: '1px 1px 12px -4px #1e1ea4;', height: 'max-content', borderRadius: '5px', display: 'flex', overflow: 'hidden'}}>
                                <Image
                                    src={getFile(novel.cover)}
                                    alt={novel.title}
                                    title={novel.title}
                                    width={sizeConfig.cardWidth}
                                    height={sizeConfig.cardHeight}

                                    // placeholder="blur" // Optional blur-up while loading
                                />
                            </Box>
                        </a>
                    </Link>
                </Box>

                <ContainerText sx={{}}>
                    <Link href={link} passHref>
                        <a style={{textDecoration: 'none'}}>
                            <Box sx={{p: 2, }}>
                                <Typography sx={{}} variant="h3">{novel.title}</Typography>
                                <Rating name="half-rating-read" size={'small'} readOnly
                                    sx={{mt: 1}}
                                    defaultValue={parseFloat(`${novel.score}`)} precision={STARS_PRECISION}
                                 />
                            </Box>
                        </a>
                    </Link>
                </ContainerText>
            </Box>

        </Box>
    )
}

const CustomText = styled(Typography)(({theme}) => ({
    padding: theme.spacing(2)
}));

const ContainerText = styled(Box)((theme) => ({
    position: 'relative',
    bottom: '25px',
    width: '100%',
    // boxShadow: `1px 1px 16px -3px ${MAIN_LIGHT}`,
    left: '10px',
    backgroundColor: '#5c596c',

    '& > *': {
        color: '#fff'
    }
}))
