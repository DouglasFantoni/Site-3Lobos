import React, {forwardRef, LegacyRef, MouseEventHandler} from "react";


interface Props {
    children: any,
    href?: string,
    onClick?: MouseEventHandler<HTMLAnchorElement> | undefined,
}

// eslint-disable-next-line react/display-name
const MyButton = forwardRef(({children, onClick, href,  }: Props, ref: LegacyRef<HTMLAnchorElement> | undefined) => {
    return (
        <a href={href} onClick={onClick} ref={ref} style={{
            boxSizing: 'border-box',
            textDecoration: 'none',
            padding: '0px',
        }}>
            {children}
        </a>
    )
})

export default  MyButton;
