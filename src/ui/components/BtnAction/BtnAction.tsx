import {Button, darken, Typography} from "@mui/material";
import {HIGHLIGHT, WHITE} from "../../../constants/ui/colors";
import Link from "next/link";
import {styled} from "@mui/system";
import BtnBase from "../BtnBase/BtnBase";

interface Props {
    link: string,
    text: string,

}
export default function BtnAction({text, link,    }: Props) {

    return (
        <Link href={link} passHref>
            <BtnBase  variant={'contained'}  >
                <Typography sx={{color: WHITE}} align={'left'} variant={"h3"}>{text}</Typography>
            </BtnBase>
        </Link>
    )
}


