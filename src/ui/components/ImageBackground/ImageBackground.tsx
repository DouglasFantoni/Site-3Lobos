import {Box} from "@mui/system";

interface Props {
    src: string,
    children?: any,
    sx?: object,
}
export default function ImageBackground({children, src, sx}: Props) {

    return (
        <Box  sx={{
            backgroundImage: `url('${src}')`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            ...sx}}>
            {children}
        </Box>
    )
}
