import { Button, darken } from "@mui/material";
import { styled } from "@mui/system";
import { HIGHLIGHT } from "../../../constants/ui/colors";

const BtnBase = styled(Button)(({ theme }) => ({
  backgroundColor: HIGHLIGHT,
  padding: `${theme.spacing(2)} ${theme.spacing(3)}`,
  borderRadius: `50px`,
  ":hover": {
    backgroundColor: darken(HIGHLIGHT, 0.1),
  },
}));

export default BtnBase;
