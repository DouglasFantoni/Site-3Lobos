import Chapter from "../../../models/chapter";
import {Box, styled} from "@mui/system";
import {Button, Typography} from "@mui/material";
import {timestampView} from "../../../utils/helpers/dataHelper";
import Link from "next/link";
import {DARK_BLUE, MAIN_LIGHT} from "../../../constants/ui/colors";


interface Props {
    chapter: Chapter,
    novelSlug: string,
}

export default function ChapterDetail({chapter, novelSlug}: Props) {

    return (
            <Link href={`/novels/${novelSlug}/capitulos/${chapter.slug}`}  passHref>
                <CustomBtn  >
                    <Box py={2}>
                        <Typography align={'left'} variant={"h3"}>{chapter.title}</Typography>
                        <Box sx={{pt: 1}}>
                            <Typography align={'left'} sx={{textTransform: 'none'}} variant={"h4"}>Postado dia {timestampView(chapter.moment)}</Typography>
                        </Box>
                    </Box>
                </CustomBtn>
            </Link>

    )
}

const textProps = {
};


const CustomBtn = styled(Button)(({theme}) => ({
    p:2,
    width: '100%',
    display: 'block',
    textAling: 'left',
    transition: 'backgroundColor 30ms ease, transform 300ms ease',
    ':hover' : {
        backgroundColor: theme.palette.mode === 'dark' ? DARK_BLUE : MAIN_LIGHT,
        transform: 'translateX(10px)'
    }
}));
