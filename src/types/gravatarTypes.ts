
export interface GrResponse {
    entry: []
}

export interface GrUserProperties{
    id: string,
    hash: string
    requestHash: string
    profileUrl: string
    preferredUsername: string
    thumbnailUrl: string
    photos: []
    name: {
        givenName: string,
        formatted: string,
    }
    displayName: string
    aboutMe: string
    urls: []
}
