import {TYPE_THEMES} from "../constants/ui/themes";


export interface SessionOptions {
    theme: TYPE_THEMES,
    fontsize: number,
}
