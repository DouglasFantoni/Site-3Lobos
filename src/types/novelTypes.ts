

export const ST_ATIVO = 'Ativo';
export const ST_FINALIZADO = 'Finalizado';
export const ST_PAUSADO = 'Pausado';


export  type NovelStatusType = 'Ativo' | 'Finalizado' | 'Pausado';

