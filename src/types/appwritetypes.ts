


export interface AppwriteResponse<P>{
    documents: Array<P>,
    sum: number
}

export interface AppwriteDocument{
    "$id": string,
    "$collection": string,
    "$permissions": AppwritePermissionType
}

export interface AppwritePermissionType {
    "read": Array<string>,
    "write": Array<string>,
}
