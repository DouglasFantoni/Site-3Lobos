import Chapter from "./chapter";


export default class Volume {
    constructor(
        public $id: string,
        public title: string,
        public idNovel: string,
        public agrupador?: string,
    ) {}
}


export class VolumesList {
        constructor(public volume: Volume, public chapters: Array<Chapter>) {
    }
}
