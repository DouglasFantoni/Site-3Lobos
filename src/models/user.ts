import UserPrefs from "./userPrefs";


export default class User {
    constructor(
        public $id: string,
        public name: string,
        public registration: string,
        public status: number,
        public passwordUpdate: string,
        public email: string,
        public emailVerification: boolean,
        // public prefs: UserPrefs
        public prefs: UserPrefs
    ) {}
}
