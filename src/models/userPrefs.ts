

export default class UserPrefs{
    constructor(
        public theme: string,
        public occupation: string,
        public description: string,
        public avatar: string,
        public social: string[],


        ) {
        }


}
