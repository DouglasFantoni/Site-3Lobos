

export default class Partner {
    constructor(
       public $id: string,
       public name: string,
       public logo: string,
       public link: string,
    ) {}
}
