import {timestampView} from "../utils/helpers/dataHelper";
import {Novel, NovelInfo} from "./novel";

export default class Chapter {
    public momentFormated: string;

    constructor(
        public $id: string,
        public title: string,
        public slug: string,
        public idVolume: string,
        public translator: string,
        public moment: number,
        public idContent: string,
        public views: number,
        public reviewer: string,
        public ordem: number,


    ) {
        this.momentFormated = timestampView(this.moment)
    }
}


export class ChapterFull extends Chapter implements NovelInfo{
    public $idNovel: string;
    public titleNovel: string;
    public coverNovel: string;
    public slugNovel: string;

    constructor(
        chapter: Chapter,
        novel: Novel,
    ) {
        super(chapter.$id,chapter.title,chapter.slug,chapter.idVolume,chapter.translator, chapter.moment,chapter.idContent, chapter.views, chapter.reviewer, chapter.ordem);

        this.$idNovel = novel.$id;
        this.titleNovel = novel.title;
        this.coverNovel = novel.cover;
        this.slugNovel = novel.slug;
    }
}

export class ChapterBasic {

    constructor(
        public id: string,
        public title: string,
        public author: string,
        public novel: string,
        public stars: number,
        public cover: string,
        public moment: string,
    ) {}
}


