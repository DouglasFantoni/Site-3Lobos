import {NovelStatusType} from "../types/novelTypes";


export class Novel{

    constructor(
        public $id: string,
        public title: string,
        public author: string,
        public synopsis: string,
        public chaptersNumber: number,
        public cover: string,
        public score: number,
        public idsGenres: Array<string>,
        public idsTags: Array<string>,
        public idNacionality: string,
        public moment: number,
        public views: number,
        public status: NovelStatusType,
        public slug: string,
        public initials: string,
        public alternativeTitle?: string,

    ) {

    }

}


export interface NovelInfo{
    $idNovel: string;
    titleNovel: string;
    coverNovel: string;
    slugNovel: string;
}
