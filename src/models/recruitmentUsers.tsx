



export default class RecruitmentUsers {
    constructor(
        public $id: string,
        public idUser: string,
        public occupation: string,
    ) {}
}
