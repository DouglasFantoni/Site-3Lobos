

export default class Content {
    constructor(
        public $id: string,
        public content: string,
    ) {}
}
