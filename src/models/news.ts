

export default class News {
    constructor(
        public $id: string,
        public title: string,
        public author: string,
        public idContent: string,
        public moment: number,
    ) {}
}

export  class NewsFull extends News{
    constructor(
        news: News,
        public content: string,
    ) {
        super (
            news.$id,
            news.title,
            news.author,
            news.idContent,
            news.moment,
        )
    }
}
