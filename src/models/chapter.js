"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.ChapterBasic = exports.ChapterFull = void 0;
var dataHelper_1 = require("../utils/helpers/dataHelper");
var Chapter = /** @class */ (function () {
    function Chapter($id, title, slug, idVolume, translator, moment, idContent, views, reviewer, ordem) {
        this.$id = $id;
        this.title = title;
        this.slug = slug;
        this.idVolume = idVolume;
        this.translator = translator;
        this.moment = moment;
        this.idContent = idContent;
        this.views = views;
        this.reviewer = reviewer;
        this.ordem = ordem;
        this.momentFormated = (0, dataHelper_1.timestampView)(this.moment);
    }
    return Chapter;
}());
exports["default"] = Chapter;
var ChapterFull = /** @class */ (function (_super) {
    __extends(ChapterFull, _super);
    function ChapterFull(chapter, novel) {
        var _this = _super.call(this, chapter.$id, chapter.title, chapter.slug, chapter.idVolume, chapter.translator, chapter.moment, chapter.idContent, chapter.views, chapter.reviewer, chapter.ordem) || this;
        _this.$idNovel = novel.$id;
        _this.titleNovel = novel.title;
        _this.coverNovel = novel.cover;
        _this.slugNovel = novel.slug;
        return _this;
    }
    return ChapterFull;
}(Chapter));
exports.ChapterFull = ChapterFull;
var ChapterBasic = /** @class */ (function () {
    function ChapterBasic(id, title, author, novel, stars, cover, moment) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.novel = novel;
        this.stars = stars;
        this.cover = cover;
        this.moment = moment;
    }
    return ChapterBasic;
}());
exports.ChapterBasic = ChapterBasic;
