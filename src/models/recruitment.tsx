



export default class Recruitment {
    constructor(
        public $id: string,
        public idNovel: string,
        public occupation: string,
        public extraInfo?: {},
    ) {}
}
