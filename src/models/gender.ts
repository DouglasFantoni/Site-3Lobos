



export default class Gender {
    constructor(
        public $id: string,
        public title: string,
    ) {}
}
