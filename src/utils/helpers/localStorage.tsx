function getStoredItem(key: string, defaultValue: any)  {
    // getting stored value
    if (typeof window !== "undefined") {
        const saved = localStorage.getItem(key);
        return saved !== null ? JSON.parse(saved) : defaultValue;
    }
}

export const storeItem = (key: string, data: any) => {
    window.localStorage.setItem(key, JSON.stringify(data));
};
