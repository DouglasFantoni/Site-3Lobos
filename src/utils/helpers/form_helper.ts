

export function getTextareaProps (name: string, formik: any){

    return {
        id: name,
        name: name,
        value: formik.values[name],
        error: formik.touched[name] && Boolean(formik.errors[name]),
        helperText: formik.touched[name] && formik.errors[name]
    }
}
