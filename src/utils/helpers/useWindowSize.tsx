import {useEffect, useState} from "react";

export interface WindowSizeState {
    width: number
    height: number
    isBreakpoint: (size: number) => {}
}

export default  function useWindowSize() {

    const [windowSize, setWindowSize] = useState<WindowSizeState>({
        width: 0,
        height: 0,
        isBreakpoint: (size: number): any => {},
    });

        useEffect(() => {

        if (typeof window !== 'undefined') {

            function handleResize() {

                setWindowSize({
                    isBreakpoint: (size: number) => window.innerWidth > size,
                    width: window.innerWidth,
                    height: window.innerHeight,
                });
            }

            window.addEventListener("resize", handleResize);

            handleResize();

            return () => window.removeEventListener("resize", handleResize);
        }

    }, []);

    return windowSize;
}
