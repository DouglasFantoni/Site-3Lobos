

export function getFile(fileId: string) {

        return (`${fileId}/view?project=${process.env.NEXT_PUBLIC_APP_PROJECT}`);
}

export function getFilePath(fileId: string) {

        return (`${process.env.NEXT_PUBLIC_REACT_APP_ENDPOINT }/storage/files/${getFile(fileId)}`);
}

