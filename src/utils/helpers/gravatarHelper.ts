import md5 from "blueimp-md5";

export default function createUserHash(userEmail: string) {

    const email = userEmail.trim().toLowerCase()

    return md5(email);
}
