"use strict";
exports.__esModule = true;
exports.timestampView = void 0;
function timestampView(timestamp) {
    var strTime = new Date(timestamp);
    return '' +
        strTime.getDate() + '/' +
        (strTime.getMonth() + 1) + '/' +
        strTime.getFullYear() + ' - ' +
        strTime.getHours() + ':' +
        strTime.getMinutes();
}
exports.timestampView = timestampView;
