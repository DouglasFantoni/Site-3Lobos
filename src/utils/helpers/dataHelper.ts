


export function timestampView(timestamp: number){

    const strTime = new Date(timestamp );

    return ''+
        strTime.getDate() + '/' +
        (strTime.getMonth()+1) + '/' +
        strTime.getFullYear() + ' - ' +
        strTime.getHours() + ':' +
        strTime.getMinutes();
}
