import {createContext, useState} from "react";
import {DARK, DEFAULT_THEME, LIGHT} from "../../constants/ui/themes";
import {useMediaQuery} from "@mui/material";
import {FONT_SIZE_DEFAULT} from "../../constants/ui/variables";
import {SessionOptions} from "../../types/theme";
import {storeItem} from "../helpers/localStorage";
import {Breakpoints, MD} from "../../constants/ui/breakpoints";

export const getSessionObject = (name = DEFAULT_THEME): SessionOptions => {
    return  {
        theme: name,
        fontsize: FONT_SIZE_DEFAULT,
    } as SessionOptions;
}


const getDefaultSession = (userPreferenceIsDark: boolean) => {
    let currentSession;

    if (typeof window !== "undefined") {

        currentSession = localStorage.getItem('appSession');
        if (currentSession)
            currentSession = JSON.parse(currentSession) as SessionOptions;
        else
            currentSession = getSessionObject(userPreferenceIsDark ? DARK.name : LIGHT.name);
    } else
        currentSession = getSessionObject(userPreferenceIsDark ? DARK.name : LIGHT.name);

    return currentSession;
}


export const SessionContext = createContext(
    {
        session: getDefaultSession(false),
        setSession: (session: SessionOptions) => {},
    },
)

interface Props {
    session?: SessionOptions | null,
    children: any,
}

export default function SessionProvider ({  children, session,}: Props) {

    const userPreferenceIsDark = useMediaQuery('(prefers-color-scheme: dark)');

    const [sessionState, _setSessionState] = useState(
session || getDefaultSession(userPreferenceIsDark)
    )

    const setSession = (session: SessionOptions)  => {
        // console.log('fontSize State', session.fontsize)
        _setSessionState(session)
        storeItem('appSession', session);
    }

    const contextValue = {
        session: sessionState,
        setSession: setSession,
    }


    // const {  width, height } = useWindowSize();

    // useEffect(() => {
    //
    //     console.log('Entrou no useEffect1: ')
    //
    //     if (width < MD){
    //         let theme = createTheme(buildTheme(themeState.theme))
    //
    //         theme = responsiveFontSizes(theme);
    //         setTheme(theme);
    //
    //     }
    //
    // }, []);

    // useEffect(() => {
    //     const restoredSettings = restoreSettings();
    //
    //     if (restoredSettings) {
    //         setCurrentSettings(restoredSettings);
    //     }
    // }, []);
   

    // console.log('Novo session: ',session)
    return (
        <SessionContext.Provider value={contextValue}>
            {children}
        </SessionContext.Provider>
    )
}

export const SessionConsumer = SessionContext.Consumer;
