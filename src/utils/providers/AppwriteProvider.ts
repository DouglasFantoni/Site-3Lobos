import {Appwrite} from "appwrite";
import sdk from "node-appwrite";

// const sdk = require('node-appwrite');

export const Server = {
    endpoint: process.env.NEXT_PUBLIC_REACT_APP_ENDPOINT,
    project: process.env.NEXT_PUBLIC_APP_PROJECT,
    secretID: process.env.REACT_APP_SECRET_KEY,
};

export let client = new sdk.Client()
    .setEndpoint(Server.endpoint as string)
    .setProject(Server.project as string)
    .setKey(Server.secretID as string)
    // .setSelfSigned() // Use only on dev mode with a self-signed SSL cert
;
// @ts-ignore
const appwrite = new Appwrite()
    .setEndpoint(Server.endpoint as string)
    .setProject(Server.project as string)



export default appwrite;

