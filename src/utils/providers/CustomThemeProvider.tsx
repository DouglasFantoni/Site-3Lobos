import {useEffect} from "react";
import {createTheme, responsiveFontSizes, ThemeProvider,} from "@mui/material";
import {buildTheme} from "../../constants/ui/themeMaterial";
import useWindowSize from "../helpers/useWindowSize";
import {MD} from "../../constants/ui/breakpoints";
import useSession from "../hooks/useSession";

export default function CustomThemeProvider  ({ children }: any) {

    const { session} = useSession();

    let personTheme = createTheme(buildTheme(session.theme));

    const {  width} = useWindowSize();
    useEffect(() => {

        // console.log('Entrou no useEffect do theme Provider: ')

        if (width < MD){
            personTheme = responsiveFontSizes(personTheme);
        }
    }, []);

    return (
            <ThemeProvider theme={personTheme}>
                {children}
            </ThemeProvider>
    )
}
