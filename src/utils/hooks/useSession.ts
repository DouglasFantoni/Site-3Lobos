import {useContext} from "react";
import {SessionContext} from "../providers/SessionProvider";


const useSettings = () => useContext(SessionContext);

export default useSettings;
