import {ImageLoaderProps} from "next/image";


export default function localImageLoader({ src, width, quality }: ImageLoaderProps) {

    return `${process.env.NEXT_PUBLIC_BASE_URL}/${src}?w=${width}&q=${quality || 75}`;
}

export function isValidHttpUrl(string: string) {
    let url;

    try {
        url = new URL(string);
    } catch (_) {
        return false;
    }

    return url.protocol === "http:" || url.protocol === "https:";
}


