const { PHASE_DEVELOPMENT_SERVER } = require('next/constants')

module.exports = (phase, { defaultConfig }) => {
    // if (phase === PHASE_DEVELOPMENT_SERVER) {
    //     return {
            /* development only config options here */
        // }
    // }

    return {
        /* config options for all phases except development here */
        pageExtensions: ['page.tsx'],
        srcDir: 'src/',
        images: {
            domains: ["150.230.73.31", 'localhost', 'https://www.gravatar.com'],
            loader: 'imgix',
            path: process.env.NEXT_PUBLIC_REACT_APP_ENDPOINT + '/storage/files/',
        },
        // async redirects() {
        //     return [
        //         {
        //             source: '/',
        //             destination: '/home',
        //             permanent: true,
        //         },
        //     ]
        // },
        async rewrites() {
            return [
                {
                    source: '/',
                    destination: '/home',
                },
            ]
        },

    }
}

