

FROM node:alpine AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /3lobos
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile


# Rebuild the source code only when needed
FROM node:alpine AS builder
WORKDIR /3lobos
COPY . .
COPY --from=deps /3lobos/node_modules ./node_modules
RUN yarn build && yarn install --development --ignore-scripts --prefer-offline

# Production image, copy all the files and run next
FROM node:alpine AS runner
WORKDIR /3lobos

ENV NODE_ENV development

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

# You only need to copy next.config.js if you are NOT using the default configuration
COPY --from=builder /3lobos/next.config.js ./
COPY --from=builder /3lobos/public ./public
COPY --from=builder --chown=nextjs:nodejs /3lobos/.next ./.next
COPY --from=builder /3lobos/node_modules ./node_modules
COPY --from=builder /3lobos/package.json ./package.json
COPY --from=builder . .

USER nextjs

EXPOSE 3000

ENV NEXT_TELEMETRY_DISABLED 1

CMD ["yarn", "dev"]
